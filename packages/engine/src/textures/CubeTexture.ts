import { TextureSrc, CubeMapTextureSrc, CubeSide } from './TextureParams';
import { BaseTexture, Init as BaseInit } from './BaseTexture';

export interface Init extends BaseInit {
  positiveX: TextureSrc;
  negativeX: TextureSrc;
  positiveY: TextureSrc;
  negativeY: TextureSrc;
  positiveZ: TextureSrc;
  negativeZ: TextureSrc;
}

export class CubeTexture extends BaseTexture {
  public readonly srcMap: CubeMapTextureSrc;

  constructor(params: Init) {
    super(params);
    this.srcMap = new Map();

    const {
      positiveX,
      negativeX,
      positiveY,
      negativeY,
      positiveZ,
      negativeZ
    } = params;

    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_POSITIVE_X, positiveX);
    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_NEGATIVE_X, negativeX);
    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_POSITIVE_Y, positiveY);
    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_NEGATIVE_Y, negativeY);
    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_POSITIVE_Z, positiveZ);
    this.srcMap.set(CubeSide.TEXTURE_CUBE_MAP_NEGATIVE_Z, negativeZ);
  }
}
