const frameBufferStack: WebGLFramebuffer[] = [];

export class FrameBuffer {
  private readonly gl: WebGL2RenderingContext;
  private readonly texture: WebGLTexture;
  private readonly depthTexture?: WebGLTexture;
  private readonly fb: WebGLFramebuffer;
  private width: number;
  private height: number;

  constructor(
    gl: WebGL2RenderingContext,
    width: number,
    height: number,
    useDepth = false
  ) {
    this.gl = gl;
    this.width = width;
    this.height = height;

    // init texture
    this.texture = this.createTexture();

    if (useDepth) {
      this.depthTexture = this.createTexture();
    }

    this.updateTexture();

    // init framebuffer
    this.fb = this.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);

    gl.framebufferTexture2D(
      gl.FRAMEBUFFER,
      gl.COLOR_ATTACHMENT0,
      gl.TEXTURE_2D,
      this.texture,
      0
    );

    if (this.depthTexture) {
      gl.framebufferTexture2D(
        gl.FRAMEBUFFER,
        gl.DEPTH_ATTACHMENT,
        gl.TEXTURE_2D,
        this.depthTexture,
        0
      );
    }

    this.unbind();
  }

  private createTexture() {
    const texture = this.gl.createTexture();
    if (texture === null) {
      throw new Error('Texture creation error');
    }
    return texture;
  }

  private createFramebuffer() {
    const fb = this.gl.createFramebuffer();
    if (fb === null) {
      throw new Error('FrameBuffer creation error');
    }
    return fb;
  }

  private updateTexture() {
    const { gl, width, height } = this;

    // color texture
    {
      gl.bindTexture(gl.TEXTURE_2D, this.texture);

      const level = 0;
      const internalFormat = gl.RGBA;
      const border = 0;
      const format = gl.RGBA;
      const type = gl.UNSIGNED_BYTE;
      const data = null;

      gl.texImage2D(
        gl.TEXTURE_2D,
        level,
        internalFormat,
        width,
        height,
        border,
        format,
        type,
        data
      );

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

      gl.bindTexture(gl.TEXTURE_2D, null);
    }

    // depth texture
    if (this.depthTexture) {
      gl.bindTexture(gl.TEXTURE_2D, this.depthTexture);

      const level = 0;
      const internalFormat = gl.DEPTH_COMPONENT24;
      const border = 0;
      const format = gl.DEPTH_COMPONENT;
      const type = gl.UNSIGNED_INT;
      const data = null;
      gl.texImage2D(
        gl.TEXTURE_2D,
        level,
        internalFormat,
        width,
        height,
        border,
        format,
        type,
        data
      );

      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

      gl.bindTexture(gl.TEXTURE_2D, this.depthTexture);
    }
  }

  bind() {
    const { gl, fb } = this;

    gl.bindFramebuffer(gl.FRAMEBUFFER, fb);

    frameBufferStack.push(fb);
  }

  unbind() {
    const { gl } = this;
    const fbs = frameBufferStack;

    fbs.pop();

    const last = fbs.length > 0 ? fbs[fbs.length - 1] : null;

    gl.bindFramebuffer(gl.FRAMEBUFFER, last);
  }

  bindTexture(type: number, unit: number) {
    const { gl } = this;

    gl.activeTexture(gl.TEXTURE0 + unit);
    gl.bindTexture(type, this.texture);
  }

  setSize(width: number, height: number) {
    this.width = width;
    this.height = height;
    this.updateTexture();
  }

  getWidth() {
    return this.width;
  }

  getHeight() {
    return this.height;
  }
}
