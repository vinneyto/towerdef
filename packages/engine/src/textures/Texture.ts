import { TextureSrc } from './TextureParams';
import { BaseTexture, Init as BaseInit } from './BaseTexture';

export interface Init extends BaseInit {
  src: TextureSrc;
}

export class Texture extends BaseTexture {
  public readonly src: TextureSrc;

  constructor(params: Init) {
    super(params);

    this.src = params.src;
  }
}
