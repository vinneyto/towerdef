import uuid from 'uuid/v4';
import {
  TextureMinFilter,
  TextureWrap,
  TextureMagFilter
} from './TextureParams';
import { trashCan } from '../trashCan';

export interface Init {
  magFilter?: TextureMagFilter;
  minFilter?: TextureMinFilter;
  wrapS?: TextureWrap;
  wrapT?: TextureWrap;
}

export class BaseTexture {
  public readonly id: string;

  public readonly magFilter: TextureMagFilter;
  public readonly minFilter: TextureMinFilter;
  public readonly wrapS: TextureWrap;
  public readonly wrapT: TextureWrap;

  constructor({ magFilter, minFilter, wrapS, wrapT }: Init) {
    this.id = uuid();

    this.magFilter =
      magFilter !== undefined ? magFilter : TextureMagFilter.LINEAR;

    this.minFilter =
      minFilter !== undefined
        ? minFilter
        : TextureMinFilter.LINEAR_MIPMAP_LINEAR;

    this.wrapS = wrapS !== undefined ? wrapS : TextureWrap.REPEAT;
    this.wrapT = wrapT !== undefined ? wrapT : TextureWrap.REPEAT;
  }

  dispose() {
    trashCan.put(this);
  }
}
