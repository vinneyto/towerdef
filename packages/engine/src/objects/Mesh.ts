import { Geometry } from '../geometry/Geometry';
import { Object3D } from './Object3D';
import { AbstractMaterial } from '../materials';

export class Mesh<
  G extends Geometry = Geometry,
  M extends AbstractMaterial = AbstractMaterial
> extends Object3D {
  public geometry: G;
  public material: M;

  constructor(geometry: G, material: M) {
    super();

    this.geometry = geometry;
    this.material = material;
  }
}
