import { Vector3, Matrix4, Quaternion } from '../math';

export interface TraverseCallback {
  (object: Object3D): void;
}

export class Object3D {
  public name?: string;

  public readonly matrix: Matrix4;
  public readonly matrixWorld: Matrix4;
  public readonly position: Vector3;
  public readonly quaternion: Quaternion;
  public readonly scale: Vector3;

  private readonly children: Object3D[];
  private visible: boolean;
  private parent: Object3D | null;

  constructor() {
    this.matrix = new Matrix4();
    this.matrixWorld = new Matrix4();
    this.position = new Vector3();
    this.quaternion = new Quaternion();
    this.scale = new Vector3(1, 1, 1);
    this.parent = null;
    this.children = [];
    this.visible = true;
  }

  add(child: Object3D) {
    if (child.parent !== null) {
      child.parent.remove(child);
    }
    this.children.push(child);
    child.parent = this;
  }

  remove(child: Object3D) {
    const index = this.children.indexOf(child);
    if (index >= 0) {
      this.children.splice(index, 1);
    }
    child.parent = null;
  }

  traverse(cb: TraverseCallback) {
    cb(this);

    for (const child of this.children) {
      child.traverse(cb);
    }
  }

  updateMatrix() {
    this.matrix.fromRotationTranslationScale(
      this.quaternion,
      this.position,
      this.scale
    );
  }

  updateMatrixWorld() {
    this.updateMatrix();

    if (this.parent === null) {
      this.matrixWorld.copy(this.matrix);
    } else {
      this.matrixWorld.multiplyMatrices(this.parent.matrixWorld, this.matrix);
    }

    for (let i = 0; i < this.children.length; i++) {
      this.children[i].updateMatrixWorld();
    }
  }

  getParent() {
    return this.parent;
  }

  getChildren() {
    return this.children;
  }

  isVisible() {
    return this.visible;
  }

  setVisible(value: boolean) {
    this.visible = value;
  }
}
