import { Color } from '../math';
import { Object3D } from './Object3D';

export enum LightType {
  Directional = 'directional',
  Point = 'point',
  Ambient = 'ambient'
}

interface Init {
  type: LightType;
  color?: Color;
  intensity?: number;
  point?: boolean;
  directional?: boolean;
  attenuationL?: number;
  attenuationQ?: number;
}

export class Light extends Object3D {
  public readonly type: LightType;
  public color: Color;
  public intensity: number;
  public attenuationL: number;
  public attenuationQ: number;

  constructor({ type, color, intensity, attenuationL, attenuationQ }: Init) {
    super();

    this.type = type;

    this.color = color !== undefined ? color : new Color(1, 1, 1);
    this.intensity = intensity !== undefined ? intensity : 1;
    this.attenuationL = attenuationL !== undefined ? attenuationL : 0.7;
    this.attenuationQ = attenuationQ !== undefined ? attenuationQ : 1.8;
  }
}
