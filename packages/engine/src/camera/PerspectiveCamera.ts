import { Vector3 } from '../math';
import { Camera } from './Camera';

const target = new Vector3();
const up = new Vector3(0, 1, 0);

export class PerspectiveCamera extends Camera {
  aspect: number;
  fovy: number;
  near: number;
  far: number;

  constructor(aspect = 1, fovy = Math.PI / 4, near = 0.1, far = 100) {
    super();

    this.aspect = aspect;
    this.fovy = fovy;
    this.near = near;
    this.far = far;

    this.updateProjectionMatrix();
  }

  updateProjectionMatrix() {
    this.projectionMatrix.perspective(
      this.fovy,
      this.aspect,
      this.near,
      this.far
    );
  }

  lookAt(x: number, y: number, z: number) {
    target.set(x, y, z);

    this.matrix.targetTo(this.position, target, up);

    this.matrix.getTranslation(this.position);
    this.matrix.getRotation(this.quaternion);
  }

  setSize(w: number, h: number) {
    this.aspect = w / h;
    this.updateProjectionMatrix();
  }
}
