import { EventEmitter } from 'events';
import { PerspectiveCamera } from './PerspectiveCamera';
import { Vector3 } from '../math/Vector3';
import { Vector2 } from '../math';

export class TurntableCameraController extends EventEmitter {
  private camera: PerspectiveCamera;
  private azimuthalAngle = 0;
  private polarAngle = 0;
  private cameraCenter: Vector3 = new Vector3(0, 0, 0);
  private zoomRadius = 10;

  constructor(fovy: number, near: number, far: number) {
    super();

    this.camera = new PerspectiveCamera(1, fovy, near, far);
  }

  private emitChange() {
    this.emit('change');
  }

  private update() {
    const theta = this.polarAngle;
    const phi = this.azimuthalAngle;
    const r = this.zoomRadius;
    const { sin, cos, abs } = Math;

    const position = new Vector3(
      r * cos(abs(theta)) * cos(phi),
      r * sin(theta),
      r * cos(abs(theta)) * sin(phi)
    );

    this.camera.position.copy(position);
    this.camera.lookAt(0, 0, 0);
    this.camera.position.add(this.cameraCenter);
    this.emitChange();
  }

  rotateDelta(delta: Vector2, factor = 1) {
    const polarAngle = this.polarAngle + delta.y * factor;
    const azimuthalAngle = this.azimuthalAngle + delta.x * factor;

    this.setRotation(polarAngle, azimuthalAngle);
  }

  setRotation(polarAngle: number, azimuthalAngle: number) {
    const { PI } = Math;
    const round = PI * 2;
    const bound = Math.PI / 2;

    this.polarAngle = Math.min(bound, Math.max(-bound, polarAngle % round));
    this.azimuthalAngle = azimuthalAngle % round;

    this.update();
  }

  getCamera() {
    return this.camera;
  }

  setCamera(camera: PerspectiveCamera) {
    this.camera = camera;
    this.update();
  }

  setCameraCenter(cameraCenter: Vector3) {
    this.cameraCenter = cameraCenter;
    this.update();
  }

  setZoomRadius(zoomRadius: number) {
    this.zoomRadius = zoomRadius;
    this.update();
  }

  setSize(w: number, h: number) {
    this.camera.setSize(w, h);
    this.emitChange();
  }
}
