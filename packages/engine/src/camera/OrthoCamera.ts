import { Camera } from './Camera';

export class OrthoCamera extends Camera {
  left: number;
  right: number;
  bottom: number;
  top: number;
  near: number;
  far: number;

  constructor(left = -1, right = 1, bottom = -1, top = 1, near = 0, far = 10) {
    super();

    this.left = left;
    this.right = right;
    this.bottom = bottom;
    this.top = top;
    this.near = near;
    this.far = far;

    this.updateProjectionMatrix();
  }

  updateProjectionMatrix() {
    this.projectionMatrix.ortho(
      this.left,
      this.right,
      this.bottom,
      this.top,
      this.near,
      this.far
    );
  }

  setAsCanvas(w: number, h: number) {
    this.left = 0;
    this.right = w;
    this.bottom = h;
    this.top = 0;

    this.updateProjectionMatrix();
  }
}
