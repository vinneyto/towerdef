import { Matrix4 } from '../math';
import { Object3D } from '../objects/Object3D';

export class Camera extends Object3D {
  public readonly viewMatrix: Matrix4;
  public readonly projectionMatrix: Matrix4;

  constructor() {
    super();

    this.viewMatrix = new Matrix4();
    this.projectionMatrix = new Matrix4();
  }

  updateMatrixWorld() {
    super.updateMatrixWorld();

    this.updateViewMatrix();
  }

  updateViewMatrix() {
    this.viewMatrix.copy(this.matrixWorld).invert();
  }
}
