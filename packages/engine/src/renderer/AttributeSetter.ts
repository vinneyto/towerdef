import { GLBufferAccessor } from './GLBufferAccessor';

export class AttributeSetter {
  private readonly location: number;

  constructor(
    private readonly gl: WebGL2RenderingContext,
    program: WebGLProgram,
    attributeInfo: WebGLActiveInfo
  ) {
    this.location = gl.getAttribLocation(program, attributeInfo.name);
  }

  set(glBufferAccessor: GLBufferAccessor) {
    const { gl, location } = this;
    const { accessor, buffer } = glBufferAccessor;
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.enableVertexAttribArray(location);
    gl.vertexAttribPointer(
      location,
      accessor.itemSize,
      accessor.componentType,
      accessor.normalize,
      accessor.bufferView.byteStride,
      accessor.byteOffset
    );
  }
}
