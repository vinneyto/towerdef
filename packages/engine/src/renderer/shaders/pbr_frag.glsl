#include<srgb_linear>

// modules --------------
const float M_PI = 3.141592653589793;

struct LightInfo {
  bool enabled;
  vec4 position;
  vec3 color;
  float attenuationL;
  float attenuationQ;
};

struct PBRInfo {
  float NdotL; // cos angle between normal and light direction
  float NdotV; // cos angle between normal and view direction
  float NdotH; // cos angle between normal and half vector
  float VdotH; // cos angle between view direction and half vector
  float alphaRoughness;
  vec3 diffuseColor;
  vec3 specularColor;
  vec3 reflectance0;
  vec3 reflectance90;
};

vec3 specularReflection(PBRInfo pbrInputs) {
  return pbrInputs.reflectance0 + (pbrInputs.reflectance90 - pbrInputs.reflectance0) * pow(clamp(1.0 - pbrInputs.VdotH, 0.0, 1.0), 5.0);
}

float geometricOcclusion(PBRInfo pbrInputs) {
  float NdotL = pbrInputs.NdotL;
  float NdotV = pbrInputs.NdotV;
  float r = pbrInputs.alphaRoughness;

  float attenuationL = 2.0 * NdotL / (NdotL + sqrt(r * r + (1.0 - r * r) * (NdotL * NdotL)));
  float attenuationV = 2.0 * NdotV / (NdotV + sqrt(r * r + (1.0 - r * r) * (NdotV * NdotV)));
  return attenuationL * attenuationV;
}

float microfacetDistribution(PBRInfo pbrInputs) {
  float roughnessSq = pbrInputs.alphaRoughness * pbrInputs.alphaRoughness;
  float f = (pbrInputs.NdotH * roughnessSq - pbrInputs.NdotH) * pbrInputs.NdotH + 1.0;
  return roughnessSq / (M_PI * f * f);
}

vec3 diffuse(PBRInfo pbrInputs) {
  return pbrInputs.diffuseColor / M_PI;
}

float attenuate(float dist, LightInfo light) {
  return 1.0 / (1.0 + dist * light.attenuationL + (dist * dist) * light.attenuationQ);
}

// uniforms ------------
uniform vec3 u_cameraPosition;
uniform vec4 u_baseColor;
uniform vec2 u_metallicRoughnessValues;
uniform vec3 ambientLightColor;
uniform LightInfo u_lights[NUM_LIGHTS];

#ifdef HAS_BASECOLORMAP
uniform sampler2D u_baseColorMap;
#endif

#ifdef HAS_NORMALMAP
uniform sampler2D u_normalMap;
uniform float u_normalScale;
#endif

#ifdef HAS_ENVIRONMENTMAP
uniform samplerCube u_environmentMap;
uniform float u_environmentMapOpacity;
uniform float u_refractionRatio;
uniform bool u_isRefraction;
#endif

#ifdef HAS_METALICROUGHNESSMAP
uniform sampler2D u_metallicRoughnessMap;
#endif

// varyings ------------
in vec3 v_mPos;
in vec2 v_uv;

#ifdef HAS_NORMALS
in vec3 v_normal;
#endif
#ifdef HAS_TANGENTS
in mat3 v_tbn;
#endif

out vec4 outColor;

// main ----------------
void main() {
  vec3 color = vec3(0);

  vec3 viewDirection = normalize(u_cameraPosition - v_mPos);

#ifdef HAS_BASECOLORMAP
  vec4 baseColor = SRGBtoLINEAR(texture(u_baseColorMap, v_uv)) * u_baseColor;
#else
  vec4 baseColor = u_baseColor;
#endif

#ifdef HAS_NORMALMAP
  vec3 n = texture(u_normalMap, v_uv).rgb;
  vec3 normal = normalize(v_tbn * ((2.0 * n - 1.0) * vec3(u_normalScale, u_normalScale, 1.0)));
#else
  vec3 normal = normalize(v_normal);
#endif

#ifdef HAS_METALICROUGHNESSMAP
  vec2 metallicRoughnessValues = texture(u_metallicRoughnessMap, v_uv).bg;
#else
  vec2 metallicRoughnessValues = u_metallicRoughnessValues;
#endif

  vec3 irradiance = vec3(0);
  float metallic = metallicRoughnessValues.x;
  float perceptualRoughness = metallicRoughnessValues.y;
  float alphaRoughness = perceptualRoughness * perceptualRoughness;

  vec3 f0 = vec3(0.04);
  vec3 diffuseColor = baseColor.rgb * (vec3(1.0) - f0);
  diffuseColor *= 1.0 - metallic;
  vec3 specularColor = mix(f0, baseColor.rgb, metallic);

  float reflectance = max(max(specularColor.r, specularColor.g), specularColor.b);
  float reflectance90 = clamp(reflectance * 25.0, 0.0, 1.0);
  vec3 specularEnvironmentR0 = specularColor.rgb;
  vec3 specularEnvironmentR90 = vec3(1.0, 1.0, 1.0) * reflectance90;

  LightInfo light;

  for (int i = 0; i < NUM_LIGHTS; i++) {
    light = u_lights[i];

    if (light.enabled) {
      vec3 n = normalize(normal);
      vec3 v = viewDirection;
      vec3 l = light.position.w == 0.0 ? vec3(light.position) : normalize(vec3(light.position) - v_mPos);
      vec3 h = normalize(l+v);

      float NdotL = clamp(dot(n, l), 0.001, 1.0);
      float NdotV = clamp(abs(dot(n, v)), 0.001, 1.0);
      float NdotH = clamp(dot(n, h), 0.0, 1.0);
      float VdotH = clamp(dot(v, h), 0.0, 1.0);

      PBRInfo pbrInputs = PBRInfo(
        NdotL,
        NdotV,
        NdotH,
        VdotH,
        alphaRoughness,
        diffuseColor,
        specularColor,
        specularEnvironmentR0,
        specularEnvironmentR90
      );

      vec3 F = specularReflection(pbrInputs);
      float G = geometricOcclusion(pbrInputs);
      float D = microfacetDistribution(pbrInputs);

      vec3 diffuseContrib = (1.0 - F) * diffuse(pbrInputs);
      vec3 specContrib = F * G * D / (4.0 * NdotL * NdotV);

      float attenuation = light.position.w == 0.0 ? 1.0 : attenuate(length(vec3(light.position) - v_mPos), light);

      irradiance += NdotL * light.color * attenuation * (diffuseContrib + specContrib);
    }
  }

  color += baseColor.rgb * ambientLightColor + irradiance;

#ifdef HAS_ENVIRONMENTMAP
  vec3 direction = u_isRefraction ? refract(-viewDirection, normal, u_refractionRatio) : reflect(-viewDirection, normal);
  color = mix(color, SRGBtoLINEAR(texture(u_environmentMap, direction)).rgb, u_environmentMapOpacity);
#endif

  outColor = LINEARtoSRGB(color, baseColor.a);
}
