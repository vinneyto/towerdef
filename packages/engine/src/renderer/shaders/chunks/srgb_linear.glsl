vec4 SRGBtoLINEAR(vec4 srgbIn) {
#ifdef MANUAL_SRGB
return vec4(pow(srgbIn.xyz,vec3(2.2)),srgbIn.w);;
#else
return srgbIn;
#endif
}

vec4 LINEARtoSRGB(vec3 color, float alpha) {
  return vec4(pow(color, vec3(1.0/2.2)), alpha);
}
