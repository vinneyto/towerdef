import srgbLinear from './srgb_linear.glsl';

const chunks = new Map<string, string>();

chunks.set('srgb_linear', srgbLinear);

export default chunks;
