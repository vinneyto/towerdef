#include<srgb_linear>

// uniforms ------------
uniform vec3 u_cameraPosition;
uniform vec4 u_baseColor;

#ifdef HAS_BASECOLORMAP
uniform sampler2D u_baseColorMap;
#endif

#ifdef HAS_ENVIRONMENTMAP
uniform samplerCube u_environmentMap;
uniform float u_environmentMapOpacity;
uniform float u_refractionRatio;
uniform bool u_isRefraction;
uniform bool u_skyBox;
#endif

// varyings ------------
in vec3 v_normal;
in vec3 v_pos;
in vec3 v_mPos;
in vec2 v_uv;

out vec4 outColor;

// main ----------------
void main() {
  vec3 color = vec3(0);
  vec3 viewDirection = normalize(u_cameraPosition - v_mPos);

#ifdef HAS_BASECOLORMAP
  vec4 baseColor = SRGBtoLINEAR(texture(u_baseColorMap, v_uv)) * u_baseColor;
#else
  vec4 baseColor = u_baseColor;
#endif

  color += baseColor.rgb;

#ifdef HAS_ENVIRONMENTMAP
  vec3 direction;
  if (u_skyBox) {
    direction = v_pos;
  } else {
    direction = u_isRefraction ? refract(-viewDirection, v_normal, u_refractionRatio) : reflect(-viewDirection, v_normal);
  }
  color = mix(color, SRGBtoLINEAR(texture(u_environmentMap, direction)).rgb, u_environmentMapOpacity);
#endif

  outColor = LINEARtoSRGB(color, baseColor.a);
}
