in vec3 a_position;
in vec2 a_uv;

#ifdef HAS_NORMALS
in vec3 a_normal;
#endif
#ifdef HAS_TANGENTS
in vec3 a_tangent;
#endif

uniform mat4 u_modelMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;
uniform mat3 u_normalMatrix;

out vec3 v_pos;
out vec3 v_mPos;
out vec2 v_uv;

#ifdef HAS_NORMALS
out vec3 v_normal;
#endif
#ifdef HAS_TANGENTS
out mat3 v_tbn;
#endif

void main() {
  vec4 pos = vec4(a_position, 1.0);
  vec4 mpos = u_modelMatrix * pos;

  v_normal = u_normalMatrix * a_normal;
  v_mPos = vec3(mpos) / mpos.w;
  v_pos = a_position;
  v_uv = a_uv;

#ifdef HAS_NORMALS
#ifdef HAS_TANGENTS
  vec3 normalW = u_normalMatrix * a_normal;
  vec3 tangentW = u_normalMatrix * a_tangent;
  vec3 bitangentW = cross(v_normal, tangentW);
  v_tbn = mat3(tangentW, bitangentW, normalW);
#else
  v_normal = u_normalMatrix * a_normal;
#endif
#endif

  gl_Position = u_projectionMatrix * u_viewMatrix * mpos;
}
