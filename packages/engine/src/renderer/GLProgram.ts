// import Texture from './Texture';
// import FrameBuffer from './FrameBuffer';
import shaderChunks from './shaders/chunks';
import { Float32ArraySetter } from './uniform-setters/Float32ArraySetter';
import { AbstractUniformSetter } from './uniform-setters/AbstractUniformSetter';
import {
  BooleanSetter,
  FloatSetter,
  IntegerSetter,
  Matrix3Setter,
  Matrix4Setter,
  Vector2Setter,
  Vector3Setter,
  Vector4Setter
} from './uniform-setters';
import { AttributeSetter } from './AttributeSetter';
import { TextureSetter } from './uniform-setters/TextureSetter';
import { AttributesMap } from './types';
import { AbstractGLTexture } from './textures/AbstractGLTexture';

export type Defines = Record<string, string | number | boolean>;

interface GLProgramInit {
  defines?: Defines;
}

const SPLIT_REGEX = /[\[\].]+/g;
const INCLUDE_REGEX = /#include ?<([_\-a-zA-Z0-9]+)>;?/g;
const DEBUG_REGES = /\/\/ ?debug/g;

export function getUniformValue(obj: unknown, key: string) {
  const chunks = key.split(SPLIT_REGEX);

  let result = obj;
  for (const chunk of chunks) {
    result = (result as Record<string, unknown>)[chunk];
  }

  return result;
}

export function includeShaderChunks(src: string) {
  return src.replace(INCLUDE_REGEX, (_a: string, chunkName: string) => {
    if (!shaderChunks.has(chunkName)) {
      throw new Error(`unknown chunk ${chunkName}`);
    }

    return `${shaderChunks.get(chunkName)}\n`;
  });
}

export function patchShader(
  src: string,
  defines: Defines,
  isFragment: boolean
): string {
  return [
    '#version 300 es',
    isFragment ? 'precision highp float;' : null,
    Object.keys(defines)
      .map(key => {
        const value = defines[key];
        if (typeof value === 'boolean' && value) {
          return `#define ${key}`;
        }
        return `#define ${key} ${value}`;
      })
      .join('\n'),
    includeShaderChunks(src)
  ]
    .filter(p => p)
    .join('\n\n');
}

export function insertRowNumbers(src: string) {
  return src
    .split('\n')
    .map((row, index) => `${index + 1}   ${row}`)
    .join('\n');
}

export function shouldDebugShader(src: string) {
  return DEBUG_REGES.test(src);
}

export function getAttributeName(name: string) {
  if (name.startsWith('a_')) {
    return name.slice(2);
  }
  return name;
}

export function getUniformName(name: string) {
  if (name.startsWith('u_')) {
    return name.slice(2);
  }
  return name;
}

export class GLProgram {
  private readonly gl: WebGL2RenderingContext;
  private readonly program: WebGLProgram;
  private readonly uniformSetters: Map<string, AbstractUniformSetter>;
  private readonly attributeSetters: Map<string, AttributeSetter>;
  private readonly options?: GLProgramInit;
  private textureUnit: number;

  constructor(
    gl: WebGL2RenderingContext,
    vertexSrc: string,
    fragmentSrc: string,
    options?: GLProgramInit
  ) {
    this.gl = gl;
    this.options = options;
    this.textureUnit = 0;

    this.program = this.createProgram(vertexSrc, fragmentSrc);

    this.uniformSetters = this.createUniformSetters();
    this.attributeSetters = this.createAttributeSetters();
  }

  private createProgram(vertexSrc: string, fragmentSrc: string) {
    const { gl } = this;

    const vertexShader = this.loadShader(vertexSrc, gl.VERTEX_SHADER);
    const fragmentShader = this.loadShader(fragmentSrc, gl.FRAGMENT_SHADER);

    const program = gl.createProgram();

    if (program === null) {
      throw new Error('Program creation error');
    }

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    gl.linkProgram(program);

    const linked = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!linked) {
      const lastError = gl.getProgramInfoLog(program);
      gl.deleteProgram(program);

      throw new Error(`Error in program linking:${lastError}`);
    }
    return program;
  }

  private loadShader(src: string, type: number) {
    const { gl, options } = this;
    const shader = gl.createShader(type);
    if (shader === null) {
      throw new Error('Shader creation error');
    }

    const defines = options && options.defines ? options.defines : {};
    const patchedSrc = patchShader(src, defines, type === gl.FRAGMENT_SHADER);
    const debug = shouldDebugShader(patchedSrc);

    if (debug) {
      console.warn(insertRowNumbers(patchedSrc));
    }

    gl.shaderSource(shader, patchedSrc);
    gl.compileShader(shader);

    const compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!compiled) {
      const lastError = gl.getShaderInfoLog(shader);
      gl.deleteShader(shader);

      console.error(`Error compiling shader :${lastError}`);
      if (!debug) {
        console.warn(insertRowNumbers(patchedSrc));
      }

      throw new Error('shader error');
    }
    return shader;
  }

  private createUniformSetter(
    uniformInfo: WebGLActiveInfo
  ): AbstractUniformSetter {
    const { gl, program } = this;
    const { type, name, size } = uniformInfo;

    // Check if this uniform is an array
    const isArray = size > 1 && name.substr(-3) === '[0]';

    if (type === gl.FLOAT && isArray) {
      return new Float32ArraySetter(gl, program, uniformInfo);
    }
    if (type === gl.FLOAT) {
      return new FloatSetter(gl, program, uniformInfo);
    }
    if (type === gl.FLOAT_VEC2) {
      return new Vector2Setter(gl, program, uniformInfo);
    }
    if (type === gl.FLOAT_VEC3) {
      return new Vector3Setter(gl, program, uniformInfo);
    }
    if (type === gl.FLOAT_VEC4) {
      return new Vector4Setter(gl, program, uniformInfo);
    }
    // if (type === gl.INT && isArray) {
    //   return v => {
    //     gl.uniform1iv(location, v);
    //   };
    // }
    if (type === gl.INT) {
      return new IntegerSetter(gl, program, uniformInfo);
    }
    // if (type === gl.INT_VEC2) {
    //   return v => {
    //     gl.uniform2iv(location, v);
    //   };
    // }
    // if (type === gl.INT_VEC3) {
    //   return v => {
    //     gl.uniform3iv(location, v);
    //   };
    // }
    // if (type === gl.INT_VEC4) {
    //   return v => {
    //     gl.uniform4iv(location, v);
    //   };
    // }
    if (type === gl.BOOL) {
      return new BooleanSetter(gl, program, uniformInfo);
    }
    // if (type === gl.BOOL_VEC2) {
    //   return v => {
    //     gl.uniform2iv(location, v);
    //   };
    // }
    // if (type === gl.BOOL_VEC3) {
    //   return v => {
    //     gl.uniform3iv(location, v);
    //   };
    // }
    // if (type === gl.BOOL_VEC4) {
    //   return v => {
    //     gl.uniform4iv(location, v);
    //   };
    // }
    // if (type === gl.FLOAT_MAT2) {
    //   return v => {
    //     gl.uniformMatrix2fv(location, false, v);
    //   };
    // }
    if (type === gl.FLOAT_MAT3) {
      return new Matrix3Setter(gl, program, uniformInfo);
    }
    if (type === gl.FLOAT_MAT4) {
      return new Matrix4Setter(gl, program, uniformInfo);
    }
    if (type === gl.SAMPLER_2D || type === gl.SAMPLER_CUBE) {
      return new TextureSetter(gl, program, uniformInfo, this.textureUnit++);
    }
    throw new Error(`unknown type: 0x${type.toString(16)}`);
  }

  private createUniformSetters() {
    const { gl, program } = this;
    // let textureUnit = 0;

    const uniformSetters = new Map<string, AbstractUniformSetter>();
    const numUniforms = gl.getProgramParameter(
      program,
      gl.ACTIVE_UNIFORMS
    ) as number;

    for (let i = 0; i < numUniforms; ++i) {
      const uniformInfo = gl.getActiveUniform(program, i);
      if (!uniformInfo) {
        break;
      }
      let { name } = uniformInfo;
      // remove the array suffix.
      if (name.substr(-3) === '[0]') {
        name = name.substr(0, name.length - 3);
      }
      const setter = this.createUniformSetter(uniformInfo);
      uniformSetters.set(name, setter);
    }

    return uniformSetters;
  }

  private createAttributeSetters() {
    const { gl, program } = this;
    const attributeSetters = new Map<string, AttributeSetter>();

    const numAttributes = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    for (let i = 0; i < numAttributes; i++) {
      const attributeInfo = gl.getActiveAttrib(program, i);
      if (!attributeInfo) {
        break;
      }
      attributeSetters.set(
        attributeInfo.name,
        new AttributeSetter(gl, program, attributeInfo)
      );
    }

    return attributeSetters;
  }

  createVertexArray(attributes: AttributesMap) {
    const { gl } = this;

    const vao = gl.createVertexArray();

    if (vao === null) {
      throw new Error('VertexArray creation error');
    }

    gl.bindVertexArray(vao);

    for (const key of this.attributeSetters.keys()) {
      const setter = this.attributeSetters.get(key);
      const info = attributes.get(getAttributeName(key));

      if (!info) {
        throw new Error(`no attribute with name ${key}`);
      }

      if (setter) {
        setter.set(info);
      }
    }

    gl.bindVertexArray(null);

    return vao;
  }

  bind() {
    const { gl } = this;

    gl.useProgram(this.program);
  }

  setUniforms(uniforms: object, textures: Map<string, AbstractGLTexture>) {
    for (const key of this.uniformSetters.keys()) {
      const setter = this.uniformSetters.get(key);

      if (setter) {
        setter.set(getUniformValue(uniforms, getUniformName(key)), textures);
      }
    }
  }
}
