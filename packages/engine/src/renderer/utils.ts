import { BufferView } from '../geometry/BufferView';

export function createBufferFromBufferView(
  gl: WebGL2RenderingContext,
  bufferView: BufferView
) {
  const buffer = gl.createBuffer();
  if (buffer === null) {
    throw new Error('Buffer creation error');
  }
  const start = bufferView.byteOffset;
  const end = start + bufferView.byteLength;
  const data =
    start !== 0 || end !== bufferView.buffer.byteLength
      ? bufferView.buffer.slice(start, end)
      : bufferView.buffer;

  gl.bindBuffer(bufferView.target, buffer);
  gl.bufferData(bufferView.target, data, gl.STATIC_DRAW);
  gl.bindBuffer(bufferView.target, null);

  return buffer;
}
