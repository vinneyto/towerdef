import { RenderPass } from './RenderPass';

export class EffectComposer {
  private readonly gl: WebGL2RenderingContext;
  private readonly passList: RenderPass[] = [];
  private width = 0;
  private height = 0;

  constructor(gl: WebGL2RenderingContext) {
    this.gl = gl;
  }

  add(pass: RenderPass) {
    this.passList.push(pass);
    return this;
  }

  render() {
    for (const pass of this.passList) {
      pass.setGL(this.gl);
      pass.setSize(this.width, this.height);
      pass.pass();
    }
  }

  setSize(width: number, height: number) {
    this.width = width;
    this.height = height;
  }
}
