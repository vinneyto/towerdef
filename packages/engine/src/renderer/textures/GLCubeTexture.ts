import { AbstractGLTexture } from './AbstractGLTexture';
import { CubeTexture } from '../../textures';

export class GLCubeTexture extends AbstractGLTexture {
  private readonly texture: WebGLTexture;

  constructor(
    gl: WebGL2RenderingContext,
    { magFilter, minFilter, wrapS, wrapT, srcMap }: CubeTexture
  ) {
    super(gl);

    this.texture = this.createTexture();

    gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.texture);

    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, magFilter);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, minFilter);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, wrapS);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, wrapT);

    const level = 0;
    const internalFormat = gl.RGBA;
    const format = gl.RGBA;
    const type = gl.UNSIGNED_BYTE;

    for (const [target, src] of srcMap.entries()) {
      gl.texImage2D(target, level, internalFormat, format, type, src);
    }
    gl.generateMipmap(gl.TEXTURE_CUBE_MAP);

    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
  }

  bind(unit: number) {
    const { gl } = this;

    gl.activeTexture(gl.TEXTURE0 + unit);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.texture);
  }

  dispose() {
    this.gl.deleteTexture(this.texture);
  }
}
