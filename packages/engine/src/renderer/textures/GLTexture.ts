import { AbstractGLTexture } from './AbstractGLTexture';
import { Texture } from '../../textures';

export class GLTexture extends AbstractGLTexture {
  private readonly texture: WebGLTexture;

  constructor(
    gl: WebGL2RenderingContext,
    { magFilter, minFilter, wrapS, wrapT, src }: Texture
  ) {
    super(gl);

    this.texture = this.createTexture();

    gl.bindTexture(gl.TEXTURE_2D, this.texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, magFilter);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, minFilter);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrapS);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrapT);

    const level = 0;
    const internalFormat = gl.RGBA;
    const format = gl.RGBA;
    const type = gl.UNSIGNED_BYTE;

    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, format, type, src);
    gl.generateMipmap(gl.TEXTURE_2D);

    gl.bindTexture(gl.TEXTURE_2D, null);
  }

  bind(unit: number) {
    const { gl } = this;

    gl.activeTexture(gl.TEXTURE0 + unit);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
  }

  dispose() {
    this.gl.deleteTexture(this.texture);
  }
}
