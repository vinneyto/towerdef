export abstract class AbstractGLTexture {
  constructor(protected readonly gl: WebGL2RenderingContext) {}

  protected createTexture() {
    const texture = this.gl.createTexture();
    if (texture === null) {
      throw new Error('Texture creation error');
    }
    return texture;
  }

  abstract bind(unit: number): void;
  abstract dispose(): void;
}
