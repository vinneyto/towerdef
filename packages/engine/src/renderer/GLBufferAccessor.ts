import { BufferAccessor } from '../geometry/BufferAccessor';

export class GLBufferAccessor {
  constructor(
    public readonly accessor: BufferAccessor,
    public readonly buffer: WebGLBuffer
  ) {}
}
