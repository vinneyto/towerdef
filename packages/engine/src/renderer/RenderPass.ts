import { FrameBuffer } from '../textures/FrameBuffer';
import { Color } from '../math';

interface RenderPassConfig {
  frameBuffer?: FrameBuffer;
  onPass: () => void;
  color?: Color;
  cleanColor?: boolean;
  cleanDepth?: boolean;
  depthTest?: boolean;
  alphaBlend?: boolean;
}

export class RenderPass {
  private gl!: WebGL2RenderingContext;
  private width!: number;
  private height!: number;

  private readonly onPass: () => void;
  private readonly frameBuffer?: FrameBuffer;
  private readonly color = new Color(1, 1, 1, 1);

  private readonly cleanColor: boolean;
  private readonly cleanDepth: boolean;
  private readonly depthTest: boolean;
  private readonly alphaBlend: boolean;

  constructor({
    onPass,
    frameBuffer,
    color,
    cleanColor = true,
    cleanDepth = true,
    depthTest = true,
    alphaBlend = true
  }: RenderPassConfig) {
    this.onPass = onPass;
    this.frameBuffer = frameBuffer;
    this.cleanColor = cleanColor;
    this.cleanDepth = cleanDepth;
    this.depthTest = depthTest;
    this.alphaBlend = alphaBlend;

    if (color) {
      this.color = color;
    }
  }

  setGL(gl: WebGL2RenderingContext) {
    this.gl = gl;
  }

  setSize(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  pass() {
    const { gl, color, onPass } = this;

    if (this.frameBuffer) {
      this.frameBuffer.bind();

      gl.viewport(
        0,
        0,
        this.frameBuffer.getWidth(),
        this.frameBuffer.getHeight()
      );
    } else {
      gl.viewport(0, 0, this.width, this.height);
    }

    if (this.cleanColor) {
      gl.clearColor(color.r, color.g, color.b, color.a);
      gl.clear(gl.COLOR_BUFFER_BIT);
    }

    if (this.cleanDepth) {
      gl.clear(gl.DEPTH_BUFFER_BIT);
    }

    if (this.depthTest) {
      gl.enable(gl.DEPTH_TEST);
    } else {
      gl.disable(gl.DEPTH_TEST);
    }

    if (this.alphaBlend) {
      gl.enable(gl.BLEND);
      gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
    } else {
      gl.disable(gl.BLEND);
    }

    onPass();

    if (this.frameBuffer) {
      this.frameBuffer.unbind();
    }
  }
}
