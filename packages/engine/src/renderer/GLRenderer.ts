import { Camera } from '../camera/Camera';
import { DrawInfo } from './DrawInfo';
import { EffectComposer } from './EffectComposer';
import { BaseTexture } from '../textures/BaseTexture';
import { trashCan, PUT_GARBAGE_EVENT } from '../trashCan';
import { Geometry, BufferView } from '../geometry';
import { Object3D, Light, Mesh, LightType } from '../objects';
import { RenderCache } from './RenderCache';
import { Uniforms, LightUniform } from '../types';
import { Matrix4, Matrix3, Vector3, Color, Vector2, Vector4 } from '../math';
import { AbstractMaterial } from '../materials';

export const NUM_LIGHTS = 6;

function objectIsVisible(object: Object3D): boolean {
  let parent: Object3D | null = object;
  while (parent !== null) {
    if (!parent.isVisible()) {
      return false;
    }
    parent = parent.getParent();
  }
  return true;
}

function createEmptyLights(count: number) {
  const lights: LightUniform[] = [];
  for (let i = 0; i < count; i++) {
    lights.push({
      enabled: false,
      position: new Vector4(),
      color: new Color(),
      attenuationL: 0,
      attenuationQ: 0
    });
  }
  return lights;
}

function createEmptyUniforms(): Uniforms {
  return {
    modelMatrix: new Matrix4(),
    viewMatrix: new Matrix4(),
    projectionMatrix: new Matrix4(),
    normalMatrix: new Matrix3(),
    normalScale: 1,
    cameraPosition: new Vector3(),
    ambientLightColor: new Color(),
    baseColor: new Color(),
    metallicRoughnessValues: new Vector2(),
    isRefraction: false,
    refractionRatio: 2.2,
    lights: createEmptyLights(NUM_LIGHTS),
    skyBox: false
  };
}

export class GLRenderer {
  private readonly canvas: HTMLCanvasElement;
  private readonly gl: WebGL2RenderingContext;
  private readonly lights: Light[];
  private readonly drawList: Mesh[];
  private readonly renderCache: RenderCache;
  private readonly uniforms: Uniforms;
  private ambientLight?: Light;

  constructor() {
    const canvas = document.createElement('canvas');

    canvas.style.position = 'absolute';
    canvas.style.width = '100%';
    canvas.style.height = '100%';

    const gl = canvas.getContext('webgl2');
    if (!gl) {
      throw new Error('Context creation error');
    }

    this.canvas = canvas;
    this.gl = gl;

    this.lights = [];
    this.drawList = [];
    this.renderCache = new RenderCache(gl);
    this.uniforms = createEmptyUniforms();

    this.collectObjects = this.collectObjects.bind(this);
    this.onSmthDisposeRequest = this.onSmthDisposeRequest.bind(this);

    trashCan.on(PUT_GARBAGE_EVENT, this.onSmthDisposeRequest);
  }

  createEffectComposer() {
    return new EffectComposer(this.gl);
  }

  hasNewSize(): boolean {
    const newWidth = this.canvas.clientWidth * devicePixelRatio;
    const newHeight = this.canvas.clientHeight * devicePixelRatio;

    if (this.canvas.width !== newWidth || this.canvas.height !== newHeight) {
      this.canvas.width = newWidth;
      this.canvas.height = newHeight;
      return true;
    }

    return false;
  }

  render(scene: Object3D, camera: Camera) {
    scene.updateMatrixWorld();
    camera.updateMatrixWorld();

    this.drawList.length = 0;
    this.lights.length = 0;
    this.ambientLight = undefined;

    scene.traverse(this.collectObjects);

    for (const mesh of this.drawList) {
      const { material, geometry } = mesh;
      const program = this.renderCache.getProgram(geometry, material);
      const drawInfo = this.renderCache.getDrawInfo(geometry, program);

      material.getUniforms(
        this.uniforms,
        mesh,
        camera,
        this.lights,
        this.ambientLight
      );

      this.initTextures();

      this.draw(material, drawInfo, this.uniforms);
    }
  }

  initTextures() {
    const {
      uniforms: {
        normalMap,
        metallicRoughnessMap,
        baseColorMap,
        environmentMap
      },
      renderCache
    } = this;
    if (normalMap !== undefined) {
      renderCache.getGLTexture(normalMap);
    }
    if (metallicRoughnessMap !== undefined) {
      renderCache.getGLTexture(metallicRoughnessMap);
    }
    if (baseColorMap !== undefined) {
      renderCache.getGLTexture(baseColorMap);
    }
    if (environmentMap !== undefined) {
      renderCache.getGLTexture(environmentMap);
    }
  }

  draw(material: AbstractMaterial, drawInfo: DrawInfo, uniforms: Uniforms) {
    const { gl } = this;
    const { geometryInfo } = drawInfo;
    const { indices, count } = geometryInfo;
    const textures = this.renderCache.getTextureCache();

    if (material.doubleSided) {
      gl.disable(gl.CULL_FACE);
    } else {
      gl.enable(gl.CULL_FACE);
    }

    drawInfo.bind();

    drawInfo.setUniforms(uniforms, textures);

    if (indices !== undefined) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indices.buffer);
      gl.drawElements(
        gl.TRIANGLES,
        indices.accessor.count,
        indices.accessor.componentType,
        0
      );
    } else {
      gl.drawArrays(gl.TRIANGLES, 0, count);
    }
  }

  collectObjects(object: Object3D) {
    if (objectIsVisible(object)) {
      if (object instanceof Mesh) {
        this.drawList.push(object);
      } else if (object instanceof Light) {
        if (
          object.type === LightType.Point ||
          object.type === LightType.Directional
        ) {
          this.lights.push(object);
        } else {
          this.ambientLight = object;
        }
      }
    }
  }

  onSmthDisposeRequest(smth: object) {
    if (smth instanceof BufferView) {
      this.renderCache.disposeBufferView(smth);
    } else if (smth instanceof BaseTexture) {
      this.renderCache.disposeTexture(smth);
    } else if (smth instanceof Geometry) {
      this.renderCache.disposeDrawInfo(smth);
    }
  }

  getContext() {
    return this.gl;
  }

  getCanvas() {
    return this.canvas;
  }

  getWidth() {
    return this.canvas.width;
  }

  setWidth(value: number) {
    this.canvas.width = value;
  }

  getHeight() {
    return this.canvas.height;
  }

  setWeight(value: number) {
    this.canvas.height = value;
  }
}
