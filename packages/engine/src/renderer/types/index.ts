import { GLBufferAccessor } from '../GLBufferAccessor';

export interface Shaders {
  vert: string;
  frag: string;
}

export type TypedArray =
  | Int8Array
  | Uint8Array
  | Int16Array
  | Uint16Array
  | Int32Array
  | Uint32Array
  | Float32Array;

export type AttributesMap = Map<string, GLBufferAccessor>;

export interface GeometryInfo {
  attributes: AttributesMap;
  indices?: GLBufferAccessor;
  count: number;
}
