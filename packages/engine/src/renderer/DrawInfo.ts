import { GLProgram } from './GLProgram';
import { GeometryInfo } from './types';
import { AbstractGLTexture } from './textures/AbstractGLTexture';

export class DrawInfo {
  public vao: WebGLVertexArrayObject;

  constructor(
    public gl: WebGL2RenderingContext,
    public program: GLProgram,
    public geometryInfo: GeometryInfo
  ) {
    this.vao = program.createVertexArray(geometryInfo.attributes);
  }

  bind() {
    this.program.bind();

    this.gl.bindVertexArray(this.vao);
  }

  setUniforms(uniforms: object, textures: Map<string, AbstractGLTexture>) {
    this.program.setUniforms(uniforms, textures);
  }

  dispose() {
    this.gl.deleteVertexArray(this.vao);
  }
}
