import { GLProgram } from './GLProgram';
import { DrawInfo } from './DrawInfo';
import { AbstractGLTexture } from './textures/AbstractGLTexture';
import { BufferView, Geometry, BufferAccessor } from '../geometry';
import { createBufferFromBufferView } from './utils';
import { BaseTexture, Texture, CubeTexture } from '../textures';
import { GLTexture } from './textures/GLTexture';
import { GLCubeTexture } from './textures/GLCubeTexture';
import { GLBufferAccessor } from './GLBufferAccessor';
import { GeometryInfo, Shaders } from './types';
import {
  AbstractMaterial,
  STANDARD_MATERIAL_TYPE,
  PBR_MATERIAL_TYPE
} from '../materials';

import standardVert from './shaders/vert.glsl';
import standardFrag from './shaders/standard_frag.glsl';
import pbrVert from './shaders/vert.glsl';
import pbrFrag from './shaders/pbr_frag.glsl';

export const NUM_LIGHTS = 6;

function getId(obj: BaseTexture | BufferView | Geometry) {
  return obj.id.slice(0, 8);
}

function getShaders(material: AbstractMaterial): Shaders {
  switch (material.type) {
    case STANDARD_MATERIAL_TYPE:
      return { vert: standardVert, frag: standardFrag };
    case PBR_MATERIAL_TYPE:
      return { vert: pbrVert, frag: pbrFrag };
    default:
      throw new Error(`unknown material type ${material.type}`);
  }
}

export class RenderCache {
  private readonly programCache: Map<string, GLProgram>;
  private readonly bufferCache: Map<string, WebGLBuffer>;
  private readonly drawInfoCache: Map<string, Map<GLProgram, DrawInfo>>;
  private readonly textureCache: Map<string, AbstractGLTexture>;

  constructor(private readonly gl: WebGL2RenderingContext) {
    this.programCache = new Map();
    this.bufferCache = new Map();
    this.drawInfoCache = new Map();
    this.textureCache = new Map();
  }

  getProgram(geometry: Geometry, material: AbstractMaterial) {
    const { gl, programCache } = this;
    const tag = material.getTag(geometry);
    const program = programCache.get(tag);

    if (program === undefined) {
      const { vert, frag } = getShaders(material);
      const defines = material.getDefines(geometry);
      const newProgram = new GLProgram(gl, vert, frag, { defines });

      console.log(`program with tag <${tag}> was created`);
      programCache.set(tag, newProgram);

      return newProgram;
    }
    return program;
  }

  getShaders(material: AbstractMaterial): Shaders {
    switch (material.type) {
      case STANDARD_MATERIAL_TYPE:
        return { vert: standardVert, frag: standardFrag };
      case PBR_MATERIAL_TYPE:
        return { vert: pbrVert, frag: pbrFrag };
      default:
        throw new Error(`unknown material type ${material.type}`);
    }
  }

  getGeometryInfo(geometry: Geometry): GeometryInfo {
    const { attributes, indices } = geometry;
    const geometryInfo: GeometryInfo = {
      attributes: new Map<string, GLBufferAccessor>(),
      count: 0
    };

    for (const [attrName, accessor] of attributes.entries()) {
      const { count } = accessor;
      if (geometryInfo.count > 0 && geometryInfo.count !== count) {
        throw new Error(
          `inappropriate number of elements for attribute '${attrName}'`
        );
      }

      geometryInfo.attributes.set(attrName, this.getGLBufferAccessor(accessor));
      geometryInfo.count = count;
    }

    if (indices !== undefined) {
      geometryInfo.indices = this.getGLBufferAccessor(indices);
    }

    return geometryInfo;
  }

  getDrawInfo(geometry: Geometry, program: GLProgram) {
    const { gl } = this;
    let perProgramCache = this.drawInfoCache.get(geometry.id);

    if (perProgramCache === undefined) {
      perProgramCache = new Map<GLProgram, DrawInfo>();
      this.drawInfoCache.set(geometry.id, perProgramCache);
    }

    let drawInfo = perProgramCache.get(program);

    if (drawInfo === undefined) {
      console.log('--- getting geometry info ---');
      const geomtryInfo = this.getGeometryInfo(geometry);
      drawInfo = new DrawInfo(gl, program, geomtryInfo);

      console.log(`drawInfo for geometry <${getId(geometry)}> was created`);
      perProgramCache.set(program, drawInfo);
    }

    return drawInfo;
  }

  disposeDrawInfo(geometry: Geometry) {
    const perProgramCache = this.drawInfoCache.get(geometry.id);
    if (perProgramCache !== undefined) {
      for (const drawInfo of perProgramCache.values()) {
        drawInfo.dispose();
      }
      this.drawInfoCache.delete(geometry.id);
    }
  }

  getGLTexture(texture: BaseTexture): AbstractGLTexture {
    const { gl, textureCache } = this;
    let glTexture = textureCache.get(texture.id);

    if (glTexture === undefined) {
      if (texture instanceof Texture) {
        glTexture = new GLTexture(gl, texture);
      } else if (texture instanceof CubeTexture) {
        glTexture = new GLCubeTexture(gl, texture);
      } else {
        throw new Error(`unknown texture type`);
      }

      console.log(`texture <${getId(texture)}> was created`);
      textureCache.set(texture.id, glTexture);
    }

    return glTexture;
  }

  disposeTexture(texture: BaseTexture) {
    const { textureCache } = this;
    const glTexture = textureCache.get(texture.id);

    if (glTexture !== undefined) {
      glTexture.dispose();

      console.log(`texture <${getId(texture)}> was disposed`);
      textureCache.delete(texture.id);
    }
  }

  getGLBufferAccessor(accessor: BufferAccessor): GLBufferAccessor {
    const buffer = this.getGLBuffer(accessor.bufferView);

    return new GLBufferAccessor(accessor, buffer);
  }

  getGLBuffer(bufferView: BufferView) {
    const { gl, bufferCache } = this;
    let glBuffer = bufferCache.get(bufferView.id);

    if (glBuffer === undefined) {
      glBuffer = createBufferFromBufferView(gl, bufferView);

      console.log(`buffer <${getId(bufferView)}> was created`);
      bufferCache.set(bufferView.id, glBuffer);
    }

    return glBuffer;
  }

  disposeBufferView(bufferView: BufferView) {
    const { gl, bufferCache } = this;
    const glBuffer = bufferCache.get(bufferView.id);

    if (glBuffer !== undefined) {
      gl.deleteBuffer(glBuffer);

      console.log(`buffer <${getId(bufferView)}> was disposed`);
      bufferCache.delete(bufferView.id);
    }
  }

  getTextureCache() {
    return this.textureCache;
  }
}
