import { AbstractUniformSetter } from './AbstractUniformSetter';

export class FloatSetter extends AbstractUniformSetter {
  set(value: unknown) {
    if (typeof value !== 'number') {
      throw this.wrongTypeError();
    }

    this.gl.uniform1f(this.location, value);
  }
}
