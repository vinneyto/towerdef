import { AbstractUniformSetter } from './AbstractUniformSetter';
import { Vector3, Color } from '../../math';

export class Vector3Setter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (value instanceof Vector3) {
      gl.uniform3fv(location, value.getElements());
    } else if (value instanceof Color) {
      gl.uniform3f(location, value.r, value.g, value.b);
    } else {
      throw this.wrongTypeError();
    }
  }
}
