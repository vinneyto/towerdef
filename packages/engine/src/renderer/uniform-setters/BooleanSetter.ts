import { AbstractUniformSetter } from './AbstractUniformSetter';

export class BooleanSetter extends AbstractUniformSetter {
  set(value: unknown) {
    if (typeof value !== 'boolean') {
      throw this.wrongTypeError();
    }
    this.gl.uniform1i(this.location, value ? 1 : 0);
  }
}
