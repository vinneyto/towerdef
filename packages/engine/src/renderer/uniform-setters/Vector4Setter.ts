import { AbstractUniformSetter } from './AbstractUniformSetter';
import { Vector4, Color } from '../../math';

export class Vector4Setter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (value instanceof Vector4) {
      gl.uniform4fv(location, value.getElements());
    } else if (value instanceof Color) {
      gl.uniform4f(location, value.r, value.g, value.b, value.a);
    } else {
      throw this.wrongTypeError();
    }
  }
}
