import { AbstractGLTexture } from '../textures/AbstractGLTexture';

export abstract class AbstractUniformSetter {
  protected location: WebGLUniformLocation | null;

  constructor(
    protected gl: WebGL2RenderingContext,
    protected program: WebGLProgram,
    protected uniformInfo: WebGLActiveInfo
  ) {
    this.location = gl.getUniformLocation(program, uniformInfo.name);
  }

  public abstract set(
    value: unknown,
    textures: Map<string, AbstractGLTexture>
  ): void;

  protected wrongTypeError() {
    return new Error(`can not set uniform: ${this.uniformInfo.name}`);
  }
}
