import { AbstractUniformSetter } from './AbstractUniformSetter';
import { Matrix3 } from '../../math';

export class Matrix3Setter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (!(value instanceof Matrix3)) {
      throw this.wrongTypeError();
    }

    gl.uniformMatrix3fv(location, false, value.getElements());
  }
}
