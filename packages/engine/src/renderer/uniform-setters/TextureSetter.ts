import { AbstractUniformSetter } from './AbstractUniformSetter';
import { BaseTexture } from '../../textures';
import { AbstractGLTexture } from '../textures/AbstractGLTexture';

export class TextureSetter extends AbstractUniformSetter {
  constructor(
    gl: WebGL2RenderingContext,
    program: WebGLProgram,
    uniformInfo: WebGLActiveInfo,
    private readonly unit: number
  ) {
    super(gl, program, uniformInfo);
  }

  set(texture: unknown, textures: Map<string, AbstractGLTexture>) {
    const { gl, location, unit } = this;
    if (!(texture instanceof BaseTexture)) {
      throw this.wrongTypeError();
    }

    const glTexture = textures.get(texture.id);

    if (glTexture === undefined) {
      throw new Error(`texture <${texture.id}> is not instantiated`);
    }

    gl.uniform1i(location, unit);

    glTexture.bind(unit);
  }
}
