import { AbstractUniformSetter } from './AbstractUniformSetter';
import { Vector2 } from '../../math';

export class Vector2Setter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (value instanceof Vector2) {
      gl.uniform2fv(location, value.getElements());
    } else {
      throw this.wrongTypeError();
    }
  }
}
