import { AbstractUniformSetter } from './AbstractUniformSetter';
import { Matrix4 } from '../../math';

export class Matrix4Setter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (!(value instanceof Matrix4)) {
      throw this.wrongTypeError();
    }

    gl.uniformMatrix4fv(location, false, value.getElements());
  }
}
