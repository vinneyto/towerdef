import { AbstractUniformSetter } from './AbstractUniformSetter';

export class IntegerSetter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    if (typeof value !== 'number') {
      throw this.wrongTypeError();
    }

    gl.uniform1i(location, value);
  }
}
