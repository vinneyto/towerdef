import { AbstractUniformSetter } from './AbstractUniformSetter';

export class Float32ArraySetter extends AbstractUniformSetter {
  set(value: unknown) {
    const { gl, location } = this;
    const { size, name } = this.uniformInfo;

    if (!(value instanceof Float32Array)) {
      throw this.wrongTypeError();
    }

    if (value.length !== size) {
      throw new Error(`wrong array size for ${name}, must be ${size}`);
    } else {
      gl.uniform1fv(location, value);
    }
  }
}
