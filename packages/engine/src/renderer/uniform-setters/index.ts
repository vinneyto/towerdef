export {
  AbstractUniformSetter as UniformSetter
} from './AbstractUniformSetter';
export { BooleanSetter } from './BooleanSetter';
export { FloatSetter } from './FloatSetter';
export { IntegerSetter } from './IntegerSetter';
export { Float32ArraySetter } from './Float32ArraySetter';
export { Vector2Setter } from './Vector2Setter';
export { Vector3Setter } from './Vector3Setter';
export { Vector4Setter } from './Vector4Setter';
export { Matrix3Setter } from './Matrix3Setter';
export { Matrix4Setter } from './Matrix4Setter';
