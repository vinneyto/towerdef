import { BufferAccessor } from '../geometry/BufferAccessor';
import { BufferAttribute } from '../geometry/BufferAttribute';
import { Geometry, AttributeName } from '../geometry/Geometry';
import { Indices } from '../geometry/Indices';

interface StandardGeometryParams {
  position?: number[];
  normal?: number[];
  uv?: number[];
  indices?: number[];
}

export function createStandardGeometry({
  position,
  normal,
  uv,
  indices
}: StandardGeometryParams) {
  const attributesMap = new Map<AttributeName, BufferAccessor>();
  if (position !== undefined) {
    attributesMap.set(
      AttributeName.Position,
      new BufferAttribute(new Float32Array(position), 3)
    );
  }
  if (normal !== undefined) {
    attributesMap.set(
      AttributeName.Normal,
      new BufferAttribute(new Float32Array(normal), 3)
    );
  }
  if (uv !== undefined) {
    attributesMap.set(
      AttributeName.UV,
      new BufferAttribute(new Float32Array(uv), 2)
    );
  }

  return new Geometry(
    attributesMap,
    indices !== undefined ? new Indices(new Uint16Array(indices)) : undefined
  );
}

export function createSphereGeometry(
  radius: number,
  subdivisionsAxis: number,
  subdivisionsHeight: number
) {
  if (subdivisionsAxis <= 0 || subdivisionsHeight <= 0) {
    throw Error('subdivisionAxis and subdivisionHeight must be > 0');
  }

  const startLatitudeInRadians = 0;
  const endLatitudeInRadians = Math.PI;
  const startLongitudeInRadians = 0;
  const endLongitudeInRadians = Math.PI * 2;

  const latRange = endLatitudeInRadians - startLatitudeInRadians;
  const longRange = endLongitudeInRadians - startLongitudeInRadians;

  const position: number[] = [];
  const normal: number[] = [];
  const uv: number[] = [];

  // Generate the individual vertices in our vertex buffer.
  for (let y = 0; y <= subdivisionsHeight; y++) {
    for (let x = 0; x <= subdivisionsAxis; x++) {
      // Generate a vertex based on its spherical coordinates
      const u = x / subdivisionsAxis;
      const v = y / subdivisionsHeight;
      const theta = longRange * u + startLongitudeInRadians;
      const phi = latRange * v + startLatitudeInRadians;
      const sinTheta = Math.sin(theta);
      const cosTheta = Math.cos(theta);
      const sinPhi = Math.sin(phi);
      const cosPhi = Math.cos(phi);
      const ux = cosTheta * sinPhi;
      const uy = cosPhi;
      const uz = sinTheta * sinPhi;
      position.push(radius * ux, radius * uy, radius * uz);
      normal.push(ux, uy, uz);
      uv.push(1 - u, v);
    }
  }

  const numVertsAround = subdivisionsAxis + 1;
  const indices: number[] = [];
  for (let x = 0; x < subdivisionsAxis; x++) {
    for (let y = 0; y < subdivisionsHeight; y++) {
      // Make triangle 1 of quad.
      indices.push(
        (y + 0) * numVertsAround + x,
        (y + 0) * numVertsAround + x + 1,
        (y + 1) * numVertsAround + x
      );

      // Make triangle 2 of quad.
      indices.push(
        (y + 1) * numVertsAround + x,
        (y + 0) * numVertsAround + x + 1,
        (y + 1) * numVertsAround + x + 1
      );
    }
  }

  return createStandardGeometry({ position, normal, uv, indices });
}

export function createTorusGeometry(
  radius: number,
  thickness: number,
  radialSubdivisions: number,
  bodySubdivisions: number,
  startAngle = 0,
  endAngle: number = Math.PI * 2
) {
  if (radialSubdivisions < 3) {
    throw new Error('radialSubdivisions must be 3 or greater');
  }

  if (bodySubdivisions < 3) {
    throw new Error('verticalSubdivisions must be 3 or greater');
  }

  const range = endAngle - startAngle;

  const radialParts = radialSubdivisions + 1;
  const bodyParts = bodySubdivisions + 1;
  const position = [];
  const normal = [];
  const uv = [];
  const indices = [];

  for (let slice = 0; slice < bodyParts; ++slice) {
    const v = slice / bodySubdivisions;
    const sliceAngle = v * Math.PI * 2;
    const sliceSin = Math.sin(sliceAngle);
    const ringRadius = radius + sliceSin * thickness;
    const ny = Math.cos(sliceAngle);
    const y = ny * thickness;
    for (let ring = 0; ring < radialParts; ++ring) {
      const u = ring / radialSubdivisions;
      const ringAngle = startAngle + u * range;
      const xSin = Math.sin(ringAngle);
      const zCos = Math.cos(ringAngle);
      const x = xSin * ringRadius;
      const z = zCos * ringRadius;
      const nx = xSin * sliceSin;
      const nz = zCos * sliceSin;
      position.push(x, y, z);
      normal.push(nx, ny, nz);
      uv.push(u, 1 - v);
    }
  }

  for (let slice = 0; slice < bodySubdivisions; ++slice) {
    for (let ring = 0; ring < radialSubdivisions; ++ring) {
      const nextRingIndex = 1 + ring;
      const nextSliceIndex = 1 + slice;
      indices.push(
        radialParts * slice + ring,
        radialParts * nextSliceIndex + ring,
        radialParts * slice + nextRingIndex
      );
      indices.push(
        radialParts * nextSliceIndex + ring,
        radialParts * nextSliceIndex + nextRingIndex,
        radialParts * slice + nextRingIndex
      );
    }
  }

  return createStandardGeometry({ position, normal, uv, indices });
}

const CUBE_FACE_INDICES = [
  [3, 7, 5, 1], // right
  [6, 2, 0, 4], // left
  [6, 7, 3, 2], // ??
  [0, 1, 5, 4], // ??
  [7, 6, 4, 5], // front
  [2, 3, 1, 0] // back
];

export function createCubeGeometry(size: number) {
  const k = size / 2;

  const cornerVertices = [
    [-k, -k, -k],
    [+k, -k, -k],
    [-k, +k, -k],
    [+k, +k, -k],
    [-k, -k, +k],
    [+k, -k, +k],
    [-k, +k, +k],
    [+k, +k, +k]
  ];

  const faceNormals = [
    [+1, +0, +0],
    [-1, +0, +0],
    [+0, +1, +0],
    [+0, -1, +0],
    [+0, +0, +1],
    [+0, +0, -1]
  ];

  const uvCoords = [[1, 0], [0, 0], [0, 1], [1, 1]];

  const position: number[] = [];
  const normal: number[] = [];
  const uv: number[] = [];
  const indices: number[] = [];

  for (let f = 0; f < 6; ++f) {
    const faceIndices = CUBE_FACE_INDICES[f];
    for (let v = 0; v < 4; ++v) {
      const p = cornerVertices[faceIndices[v]];
      const n = faceNormals[f];
      const u = uvCoords[v];

      position.push(...p);
      normal.push(...n);
      uv.push(...u);
    }
    const offset = 4 * f;
    indices.push(offset + 0, offset + 1, offset + 2);
    indices.push(offset + 0, offset + 2, offset + 3);
  }

  return createStandardGeometry({ position, normal, uv, indices });
}
