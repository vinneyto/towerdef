import { EventEmitter } from 'events';
import { Vector2 } from '../math';

export class ViewInteractor extends EventEmitter {
  private dragging: boolean;
  private readonly lastPoint: Vector2;

  constructor(private readonly element: HTMLElement) {
    super();

    this.dragging = false;
    this.lastPoint = new Vector2();

    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);

    this.addListeners();
  }

  private addListeners() {
    this.element.addEventListener('mousedown', this.onMouseDown);
    this.element.addEventListener('mousemove', this.onMouseMove);
    this.element.addEventListener('mouseup', this.onMouseUp);
  }

  private onMouseDown(e: MouseEvent) {
    this.dragging = true;
    this.lastPoint.set(e.pageX, e.pageY);
  }

  private onMouseMove(e: MouseEvent) {
    if (this.dragging) {
      const delta = new Vector2(e.pageX, e.pageY).sub(this.lastPoint);
      this.lastPoint.set(e.pageX, e.pageY);
      this.emit('dragging', delta);
    }
  }

  private onMouseUp() {
    this.dragging = false;
  }
}
