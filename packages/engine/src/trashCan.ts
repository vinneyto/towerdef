import { EventEmitter } from 'events';

export const PUT_GARBAGE_EVENT = 'put_garbage';

class TrashCan extends EventEmitter {
  put(smth: object) {
    this.emit(PUT_GARBAGE_EVENT, smth);
  }
}

const trashCan = new TrashCan();

export { trashCan };
