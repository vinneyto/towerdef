import { Matrix4, Matrix3, Vector3, Color, Vector2, Vector4 } from '../math';
import { BaseTexture } from '../textures';

export type Defines = Record<string, string | number | boolean>;

export interface LightUniform {
  enabled: boolean;
  position: Vector4;
  color: Color;
  attenuationL: number;
  attenuationQ: number;
}

export interface Uniforms {
  modelMatrix: Matrix4;
  viewMatrix: Matrix4;
  projectionMatrix: Matrix4;
  normalMatrix: Matrix3;
  cameraPosition: Vector3;
  skyBox: boolean;
  baseColor: Color;
  baseColorMap?: BaseTexture;
  environmentMap?: BaseTexture;
  environmentMapOpacity?: number;
  isRefraction: boolean;
  refractionRatio: number;
  lights: LightUniform[];
  ambientLightColor: Color;
  metallicRoughnessValues: Vector2;
  metallicRoughnessMap?: BaseTexture;
  normalMap?: BaseTexture;
  normalScale: number;
}
