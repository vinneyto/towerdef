import { BufferAccessor, ComponentType } from './BufferAccessor';
import { BufferView, BufferViewTarget } from './BufferView';

export class BufferAttribute extends BufferAccessor {
  constructor(data: Float32Array, itemSize: number) {
    const bufferView = new BufferView(
      data.buffer as ArrayBuffer,
      data.byteLength,
      BufferViewTarget.ARRAY_BUFFER,
      0,
      0
    );
    super(
      bufferView,
      ComponentType.FLOAT,
      Math.floor(data.length / itemSize),
      itemSize,
      0,
      false
    );
  }
}
