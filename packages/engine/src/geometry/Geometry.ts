import uuid from 'uuid/v4';
import { trashCan } from '../trashCan';
import { BufferAccessor } from './BufferAccessor';

export enum AttributeName {
  Position = 'position',
  Normal = 'normal',
  UV = 'uv',
  Tangent = 'tangent'
}

export class Geometry {
  public readonly id: string;

  constructor(
    public readonly attributes: Map<AttributeName, BufferAccessor>,
    public readonly indices?: BufferAccessor
  ) {
    this.id = uuid();
  }

  dispose() {
    trashCan.put(this);
  }
}
