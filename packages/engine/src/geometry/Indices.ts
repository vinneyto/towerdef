import { BufferAccessor, ComponentType } from './BufferAccessor';
import { BufferView, BufferViewTarget } from './BufferView';

export class Indices extends BufferAccessor {
  constructor(data: Uint16Array) {
    const bufferView = new BufferView(
      data.buffer as ArrayBuffer,
      data.byteLength,
      BufferViewTarget.ELEMENT_ARRAY_BUFFER,
      0,
      0
    );

    super(bufferView, ComponentType.UNSIGNED_SHORT, data.length, 1, 0, false);
  }
}
