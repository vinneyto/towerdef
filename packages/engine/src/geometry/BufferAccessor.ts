import { BufferView } from './BufferView';

export enum ComponentType {
  BYTE = 5120,
  UNSIGNED_BYTE = 5121,
  SHORT = 5122,
  UNSIGNED_SHORT = 5123,
  UNSIGNED_INT = 5125,
  FLOAT = 5126
}

export class BufferAccessor {
  constructor(
    public readonly bufferView: BufferView,
    public readonly componentType: ComponentType,
    public readonly count: number,
    public readonly itemSize: number,
    public readonly byteOffset = 0,
    public readonly normalize = false
  ) {}
}
