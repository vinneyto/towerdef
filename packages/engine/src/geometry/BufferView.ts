import uuid from 'uuid/v4';
import { trashCan } from '../trashCan';

export enum BufferViewTarget {
  ARRAY_BUFFER = 34962,
  ELEMENT_ARRAY_BUFFER = 34963
}

export class BufferView {
  public readonly id: string;

  constructor(
    public readonly buffer: ArrayBuffer,
    public readonly byteLength: number,
    public readonly target: BufferViewTarget,
    public readonly byteStride = 0,
    public readonly byteOffset = 0
  ) {
    this.id = uuid();
  }

  dispose() {
    trashCan.put(this);
  }
}
