import {
  AbstractMaterial,
  AbstractMaterialInit,
  ifdef,
  setupBaseUniforms
} from './AbstractMaterial';
import { Color } from '../math';
import { Texture, CubeTexture } from '../textures';
import { EnvironmentType } from './PBRMaterial';
import { Geometry, AttributeName } from '../geometry';
import { Uniforms } from '../types';
import { Mesh } from '../objects';
import { Camera } from '../camera';

export const STANDARD_MATERIAL_TYPE = 'STANDARD_MATERIAL';

interface Init extends AbstractMaterialInit {
  skyBox?: boolean;
  baseColor?: Color;
  baseColorMap?: Texture;
  environmentMap?: CubeTexture;
  environmentMapOpacity?: number;
  environmentType?: EnvironmentType;
  refractionRatio?: number;
}

export class StandardMaterial extends AbstractMaterial {
  public readonly type = STANDARD_MATERIAL_TYPE;
  public skyBox: boolean;
  public baseColor: Color;
  public baseColorMap?: Texture;

  public environmentMap?: CubeTexture;
  public environmentMapOpacity: number;
  public environmentType: EnvironmentType;
  public refractionRatio: number;

  constructor(params: Init) {
    super(params);

    const {
      skyBox,
      baseColor,
      baseColorMap,
      environmentMap,
      environmentMapOpacity,
      environmentType,
      refractionRatio
    } = params;

    this.skyBox = skyBox !== undefined ? skyBox : false;
    this.baseColor = baseColor !== undefined ? baseColor : new Color(1, 1, 1);
    this.baseColorMap = baseColorMap;

    this.environmentMap = environmentMap;
    this.environmentMapOpacity =
      environmentMapOpacity !== undefined ? environmentMapOpacity : 1;

    this.environmentType =
      environmentType !== undefined
        ? environmentType
        : EnvironmentType.REFLECTION;
    this.refractionRatio =
      refractionRatio !== undefined ? refractionRatio : 0.98;
  }

  getTag(geometry: Geometry) {
    let tag = 'standard:';
    if (this.baseColorMap !== undefined) {
      tag += 'baseColorMap:';
    }
    if (this.environmentMap !== undefined) {
      tag += 'environmentMap:';
    }
    if (this.manualSRGB === true) {
      tag += 'manualSRGB:';
    }
    if (geometry.attributes.has(AttributeName.Normal)) {
      tag += 'normals:';
    }
    return tag;
  }

  getDefines(geometry: Geometry) {
    return {
      ...ifdef({
        HAS_BASECOLORMAP: this.baseColorMap !== undefined,
        HAS_ENVIRONMENTMAP: this.environmentMap !== undefined,
        HAS_NORMALS: geometry.attributes.has(AttributeName.Normal)
      }),
      MANUAL_SRGB: this.manualSRGB
    };
  }

  getUniforms(uniforms: Uniforms, mesh: Mesh, camera: Camera) {
    setupBaseUniforms(uniforms, mesh, camera);

    uniforms.skyBox = this.skyBox;
    uniforms.baseColor.copy(this.baseColor);

    uniforms.baseColorMap = this.baseColorMap;

    uniforms.environmentMap = this.environmentMap;
    uniforms.environmentMapOpacity = this.environmentMapOpacity;
    uniforms.refractionRatio = this.refractionRatio;
    uniforms.isRefraction = this.environmentType === EnvironmentType.REFRACTION;
  }
}
