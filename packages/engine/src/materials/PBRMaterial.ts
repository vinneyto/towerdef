import {
  AbstractMaterial,
  AbstractMaterialInit,
  ifdef,
  setupBaseUniforms
} from './AbstractMaterial';
import { Color } from '../math';
import { Texture, CubeTexture } from '../textures';
import { Uniforms, LightUniform } from '../types';
import { Light, LightType, Mesh } from '../objects';
import { Camera } from '../camera';
import { Geometry, AttributeName } from '../geometry';

export const PBR_MATERIAL_TYPE = 'PBR_MATERIAL';

export const NUM_LIGHTS = 6;

export enum EnvironmentType {
  REFLECTION,
  REFRACTION
}

function setupLight(light: LightUniform, object: Light) {
  light.enabled = true;
  light.color.copy(object.color).scale(object.intensity);
  switch (object.type) {
    case LightType.Point:
      light.position
        .copy3(object.position, 1)
        .transformMat4(object.matrixWorld);
      break;
    case LightType.Directional:
      light.position.copy3(object.position, 0);
      break;
  }
  light.attenuationL = object.attenuationL;
  light.attenuationQ = object.attenuationQ;
}

function disableLight(light: LightUniform) {
  light.enabled = false;
  light.color.rgb(0, 0, 0);
  light.position.set(0, 0, 0, 1);
  light.attenuationL = 0;
  light.attenuationQ = 0;
}

interface Init extends AbstractMaterialInit {
  baseColor?: Color;
  metallic?: number;
  roughness?: number;
  metallicRoughnessMap?: Texture;
  baseColorMap?: Texture;
  normalMap?: Texture;
  normalScale?: number;
  environmentMap?: CubeTexture;
  environmentMapOpacity?: number;
  environmentType?: EnvironmentType;
  refractionRatio?: number;
}

export class PBRMaterial extends AbstractMaterial {
  public readonly type = PBR_MATERIAL_TYPE;

  public baseColor: Color;
  public metallic: number;
  public roughness: number;
  public metallicRoughnessMap?: Texture;

  public baseColorMap?: Texture;
  public normalMap?: Texture;
  public normalScale: number;

  public environmentMap?: CubeTexture;
  public environmentMapOpacity: number;
  public environmentType: EnvironmentType;
  public refractionRatio: number;

  constructor(params: Init) {
    super(params);

    const {
      baseColor,
      metallic,
      roughness,
      metallicRoughnessMap,
      baseColorMap,
      environmentMap,
      environmentMapOpacity,
      environmentType,
      refractionRatio,
      normalMap,
      normalScale
    } = params;

    this.baseColor = baseColor !== undefined ? baseColor : new Color(1, 1, 1);
    this.metallic = metallic !== undefined ? metallic : 0.1;
    this.roughness = roughness !== undefined ? roughness : 0.3;
    this.metallicRoughnessMap = metallicRoughnessMap;

    this.baseColorMap = baseColorMap;
    this.normalMap = normalMap;
    this.normalScale = normalScale !== undefined ? normalScale : 1;

    this.environmentMap = environmentMap;
    this.environmentMapOpacity =
      environmentMapOpacity !== undefined ? environmentMapOpacity : 1;

    this.environmentType =
      environmentType !== undefined
        ? environmentType
        : EnvironmentType.REFLECTION;
    this.refractionRatio =
      refractionRatio !== undefined ? refractionRatio : 0.98;
  }

  getTag(geometry: Geometry) {
    let tag = 'pbr:';
    if (this.baseColorMap !== undefined) {
      tag += 'baseColorMap:';
    }
    if (this.normalMap !== undefined) {
      tag += 'normalMap:';
    }
    if (this.environmentMap !== undefined) {
      tag += 'environmentMap:';
    }
    if (this.metallicRoughnessMap !== undefined) {
      tag += 'metallicRoughnessMap:';
    }
    if (this.manualSRGB === true) {
      tag += 'manualSRGB:';
    }
    if (geometry.attributes.has(AttributeName.Normal)) {
      tag += 'normals:';
    }
    if (geometry.attributes.has(AttributeName.Tangent)) {
      tag += 'tangents:';
    }
    return tag;
  }

  getDefines(geometry: Geometry) {
    return {
      ...ifdef({
        HAS_BASECOLORMAP: this.baseColorMap !== undefined,
        HAS_NORMALMAP: this.normalMap !== undefined,
        HAS_ENVIRONMENTMAP: this.environmentMap !== undefined,
        HAS_METALICROUGHNESSMAP: this.metallicRoughnessMap !== undefined,
        HAS_NORMALS: geometry.attributes.has(AttributeName.Normal),
        HAS_TANGENTS: geometry.attributes.has(AttributeName.Tangent)
      }),
      NUM_LIGHTS,
      MANUAL_SRGB: this.manualSRGB
    };
  }

  getUniforms(
    uniforms: Uniforms,
    mesh: Mesh,
    camera: Camera,
    lights: Light[],
    ambientLight?: Light
  ) {
    const { metallic, roughness } = this;

    setupBaseUniforms(uniforms, mesh, camera);

    uniforms.metallicRoughnessValues.set(metallic, roughness);
    uniforms.baseColor.copy(this.baseColor);

    uniforms.baseColorMap = this.baseColorMap;
    uniforms.normalMap = this.normalMap;
    uniforms.normalScale = this.normalScale;

    uniforms.environmentMap = this.environmentMap;
    uniforms.environmentMapOpacity = this.environmentMapOpacity;
    uniforms.refractionRatio = this.refractionRatio;
    uniforms.isRefraction = this.environmentType === EnvironmentType.REFRACTION;

    uniforms.metallicRoughnessMap = this.metallicRoughnessMap;

    if (lights.length > NUM_LIGHTS) {
      console.warn('num lights is exceeded');
    }

    for (let i = 0; i < NUM_LIGHTS; i++) {
      const object = lights[i];
      const light = uniforms.lights[i];

      if (object) {
        setupLight(light, object);
      } else {
        disableLight(light);
      }
    }

    if (ambientLight !== undefined) {
      uniforms.ambientLightColor
        .copy(ambientLight.color)
        .scale(ambientLight.intensity);
    } else {
      uniforms.ambientLightColor.rgb(0, 0, 0);
    }
  }
}
