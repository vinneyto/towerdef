import { Camera } from '../camera';
import { Mesh, Light } from '../objects';
import { Uniforms, Defines } from '../types';
import { Geometry } from '../geometry';

export interface AbstractMaterialInit {
  doubleSide?: boolean;
  manualSRGB?: boolean;
}

export function setupBaseUniforms(
  uniforms: Uniforms,
  mesh: Mesh,
  camera: Camera
) {
  uniforms.modelMatrix.copy(mesh.matrixWorld);
  uniforms.viewMatrix.copy(camera.viewMatrix);
  uniforms.projectionMatrix.copy(camera.projectionMatrix);
  uniforms.normalMatrix
    .fromMat4(mesh.matrixWorld)
    .invert()
    .transpose();
  uniforms.cameraPosition.copy(camera.position);
}

export function ifdef(obj: Record<string, boolean>) {
  const result: Record<string, boolean> = {};
  for (const key of Object.keys(obj)) {
    if (obj[key]) {
      result[key] = true;
    }
  }
  return result;
}

export abstract class AbstractMaterial {
  abstract readonly type: string;

  public name?: string;
  public doubleSided: boolean;
  public manualSRGB: boolean;

  constructor({ doubleSide, manualSRGB }: AbstractMaterialInit = {}) {
    this.doubleSided = doubleSide !== undefined ? doubleSide : false;
    this.manualSRGB = manualSRGB !== undefined ? manualSRGB : true;
  }

  abstract getTag(geometry: Geometry): string;

  abstract getDefines(geometry: Geometry): Defines;

  abstract getUniforms(
    uniforms: Uniforms,
    mesh: Mesh,
    camera: Camera,
    lights: Light[],
    ambientLight?: Light
  ): void;
}
