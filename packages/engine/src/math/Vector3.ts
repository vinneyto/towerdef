import { vec3 } from 'gl-matrix';
import { Matrix4 } from './Matrix4';

export class Vector3 {
  private readonly elements: vec3;

  constructor(x = 0, y = 0, z = 0) {
    this.elements = vec3.fromValues(x, y, z);
  }

  set(x: number, y: number, z: number) {
    vec3.set(this.elements, x, y, z);
    return this;
  }

  normalize() {
    vec3.normalize(this.elements, this.elements);
    return this;
  }

  scale(v: number) {
    vec3.scale(this.elements, this.elements, v);
    return this;
  }

  add(v: Vector3) {
    vec3.add(this.elements, this.elements, v.elements);
    return this;
  }

  sub(v: Vector3) {
    vec3.sub(this.elements, this.elements, v.elements);
    return this;
  }

  dot(v: Vector3) {
    return vec3.dot(this.elements, v.elements);
  }

  transformMat4(m: Matrix4) {
    vec3.transformMat4(this.elements, this.elements, m.getElements());
    return this;
  }

  reflect(n: Vector3) {
    const b = n
      .clone()
      .scale(2)
      .scale(this.dot(n));
    return this.sub(b);
  }

  getElements() {
    return this.elements;
  }

  copy(v: Vector3) {
    vec3.copy(this.elements, v.elements);
    return this;
  }

  clone() {
    const v = new Vector3();
    v.copy(this);
    return v;
  }

  equals(v: Vector3) {
    return vec3.equals(this.elements, v.elements);
  }

  length() {
    return vec3.length(this.elements);
  }

  get x() {
    return this.elements[0];
  }

  set x(value: number) {
    this.elements[0] = value;
  }

  get y() {
    return this.elements[1];
  }

  set y(value: number) {
    this.elements[1] = value;
  }

  get z() {
    return this.elements[2];
  }

  set z(value: number) {
    this.elements[2] = value;
  }
}
