import { mat3 } from 'gl-matrix';
import { Matrix4 } from './Matrix4';

export class Matrix3 {
  private readonly elements: mat3;

  constructor() {
    this.elements = mat3.create();
  }

  fromMat4(matrix: Matrix4) {
    mat3.fromMat4(this.elements, matrix.getElements());
    return this;
  }

  invert() {
    mat3.invert(this.elements, this.elements);
    return this;
  }

  transpose() {
    mat3.transpose(this.elements, this.elements);
    return this;
  }

  getElements() {
    return this.elements;
  }
}
