import { vec4 } from 'gl-matrix';
import { Matrix4 } from './Matrix4';
import { Vector3 } from './Vector3';

export class Vector4 {
  private readonly elements: vec4;

  constructor(x = 0, y = 0, z = 0, w = 1) {
    this.elements = vec4.fromValues(x, y, z, w);
  }

  set(x: number, y: number, z: number, w: number) {
    vec4.set(this.elements, x, y, z, w);
    return this;
  }

  normalize() {
    vec4.normalize(this.elements, this.elements);
    return this;
  }

  transformMat4(m: Matrix4) {
    vec4.transformMat4(this.elements, this.elements, m.getElements());
    return this;
  }

  copy(v: Vector4) {
    vec4.copy(this.elements, v.elements);
    return this;
  }

  copy3(v: Vector3, w = 1.0) {
    vec4.set(this.elements, v.x, v.y, v.z, w);
    return this;
  }

  getElements() {
    return this.elements;
  }

  get x() {
    return this.elements[0];
  }

  set x(value: number) {
    this.elements[0] = value;
  }

  get y() {
    return this.elements[1];
  }

  set y(value: number) {
    this.elements[1] = value;
  }

  get z() {
    return this.elements[2];
  }

  set z(value: number) {
    this.elements[2] = value;
  }

  get w() {
    return this.elements[3];
  }

  set w(value: number) {
    this.elements[3] = value;
  }
}
