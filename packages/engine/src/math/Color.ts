export class Color {
  constructor(public r = 0, public g = 0, public b = 0, public a = 1) {}

  copy(c: Color) {
    this.set(c.r, c.g, c.b, c.a);
    return this;
  }

  set(r: number, g: number, b: number, a: number) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
    return this;
  }

  rgb(r: number, g: number, b: number) {
    this.r = r;
    this.g = g;
    this.b = b;
    return this;
  }

  scale(value: number) {
    this.r *= value;
    this.g *= value;
    this.b *= value;
    return this;
  }

  toLinear() {
    const gamma = 2.2;
    this.r = this.r ** gamma;
    this.g = this.g ** gamma;
    this.b = this.b ** gamma;
    return this;
  }

  toSRGB() {
    const gamma = 1 / 2.2;
    this.r = this.r ** gamma;
    this.g = this.g ** gamma;
    this.b = this.b ** gamma;
    return this;
  }

  fromHex(str: string) {
    const hex = str.replace('#', '');
    this.r = parseInt(hex.substring(0, 2), 16) / 255;
    this.g = parseInt(hex.substring(2, 4), 16) / 255;
    this.b = parseInt(hex.substring(4, 6), 16) / 255;
    return this;
  }
}
