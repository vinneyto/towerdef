import { mat4 } from 'gl-matrix';
import { Vector3 } from './Vector3';
import { Quaternion } from './Quaternion';

export class Matrix4 {
  private readonly elements: mat4;

  constructor() {
    this.elements = mat4.create();
  }

  copy(matrix: Matrix4) {
    mat4.copy(this.elements, matrix.elements);
    return this;
  }

  invert() {
    mat4.invert(this.elements, this.elements);
    return this;
  }

  transpose() {
    mat4.transpose(this.elements, this.elements);
    return this;
  }

  identity() {
    mat4.identity(this.elements);
    return this;
  }

  translate(v: Vector3) {
    mat4.translate(this.elements, this.elements, v.getElements());
    return this;
  }

  perspective(fovy: number, aspect: number, near: number, far: number) {
    mat4.perspective(this.elements, fovy, aspect, near, far);
    return this;
  }

  ortho(
    left: number,
    right: number,
    bottom: number,
    top: number,
    near: number,
    far: number
  ) {
    mat4.ortho(this.elements, left, right, bottom, top, near, far);
    return this;
  }

  targetTo(eye: Vector3, target: Vector3, up: Vector3) {
    // @ts-ignore
    mat4.targetTo(this.elements, eye.elements, target.elements, up.elements);
    return this;
  }

  multiply(m: Matrix4) {
    mat4.multiply(this.elements, this.elements, m.elements);
    return this;
  }

  multiplyMatrices(a: Matrix4, b: Matrix4) {
    mat4.multiply(this.elements, a.elements, b.elements);
    return this;
  }

  fromTranslation(v: Vector3) {
    mat4.fromTranslation(this.elements, v.getElements());
    return this;
  }

  fromRotation(w: number, axis: Vector3) {
    mat4.fromRotation(this.elements, w, axis.getElements());
    return this;
  }

  fromRotationTranslation(q: Quaternion, v: Vector3) {
    mat4.fromRotationTranslation(
      this.elements,
      q.getElements(),
      v.getElements()
    );
    return this;
  }

  fromRotationTranslationScale(q: Quaternion, v: Vector3, s: Vector3) {
    mat4.fromRotationTranslationScale(
      this.elements,
      q.getElements(),
      v.getElements(),
      s.getElements()
    );
    return this;
  }

  getTranslation(v: Vector3) {
    mat4.getTranslation(v.getElements(), this.elements);
    return this;
  }

  getRotation(q: Quaternion) {
    mat4.getRotation(q.getElements(), this.elements);
    return this;
  }

  getElements() {
    return this.elements;
  }
}
