import { quat } from 'gl-matrix';

export class Quaternion {
  private readonly elements: quat;

  constructor() {
    this.elements = quat.create();
  }

  set(x: number, y: number, z: number, w: number) {
    quat.set(this.elements, x, y, z, w);
    return this;
  }

  multiply(q: Quaternion) {
    quat.multiply(this.elements, this.elements, q.elements);
    return this;
  }

  fromEuler(x: number, y: number, z: number) {
    quat.fromEuler(this.elements, x, y, z);
    return this;
  }

  getElements() {
    return this.elements;
  }

  copy(q: Quaternion) {
    quat.copy(this.elements, q.elements);
    return this;
  }
}
