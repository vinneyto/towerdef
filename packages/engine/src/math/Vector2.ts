import { vec2 } from 'gl-matrix';

export class Vector2 {
  private readonly elements: vec2;

  constructor(x = 0, y = 0) {
    this.elements = vec2.create();

    this.set(x, y);
  }

  set(x: number, y: number) {
    vec2.set(this.elements, x, y);
    return this;
  }

  normalize() {
    vec2.normalize(this.elements, this.elements);
    return this;
  }

  sub(v: Vector2) {
    vec2.sub(this.elements, this.elements, v.elements);
    return this;
  }

  copy(v: Vector2) {
    vec2.copy(this.elements, v.elements);
    return this;
  }

  getElements() {
    return this.elements;
  }

  get x() {
    return this.elements[0];
  }

  set x(value: number) {
    this.elements[0] = value;
  }

  get y() {
    return this.elements[1];
  }

  set y(value: number) {
    this.elements[1] = value;
  }
}
