import {
  Object3D,
  BufferView,
  BufferViewTarget,
  BufferAccessor,
  ComponentType,
  Mesh,
  Geometry,
  PBRMaterial,
  Color,
  AttributeName,
  TextureMagFilter,
  TextureMinFilter,
  TextureWrap,
  Texture
} from '@towerdef/engine';
import { GLTF } from './types';
import { GLTFAccessorType } from './types/GLTFAccessor';

export function renderGLTF(gltf: GLTF): Object3D[] {
  const bufferViews = createBufferViews(gltf);
  const textures = createTextures(gltf);
  const materials = createMaterials(gltf, textures);
  const accessors = createAccessors(gltf, bufferViews);
  const meshes = createMeshes(gltf, accessors, materials);
  const nodes = createNodes(gltf, meshes);
  const scenes = createScenes(gltf, nodes);

  return scenes;
}

function getBufferViewTarget(gltf: GLTF, index: number): BufferViewTarget {
  if (gltf.root.bufferViews === undefined) {
    return BufferViewTarget.ARRAY_BUFFER;
  }

  const bv = gltf.root.bufferViews[index];

  if (bv.target !== undefined) {
    return bv.target as BufferViewTarget;
  }

  if (gltf.root.meshes === undefined || gltf.root.accessors === undefined) {
    return BufferViewTarget.ARRAY_BUFFER;
  }

  for (const m of gltf.root.meshes) {
    for (const p of m.primitives) {
      if (p.indices !== undefined) {
        const a = gltf.root.accessors[p.indices];
        if (a.bufferView === index) {
          return BufferViewTarget.ELEMENT_ARRAY_BUFFER;
        }
      }
    }
  }

  return BufferViewTarget.ARRAY_BUFFER;
}

function createBufferViews(gltf: GLTF) {
  return (gltf.root.bufferViews || []).map((bv, index) => {
    const buffer = gltf.buffers[bv.buffer];
    const target = getBufferViewTarget(gltf, index);

    return new BufferView(
      buffer,
      bv.byteLength,
      target,
      bv.byteStride,
      bv.byteOffset
    );
  });
}

function createAccessors(gltf: GLTF, bufferViews: BufferView[]) {
  return (gltf.root.accessors || []).map(a => {
    if (a.bufferView === undefined) {
      throw new Error('buffer view is not defened');
    }

    const bufferView = bufferViews[a.bufferView];
    const componentType = a.componentType as ComponentType;
    const itemSize = getItemSize(a.type);

    return new BufferAccessor(
      bufferView,
      componentType,
      a.count,
      itemSize,
      a.byteOffset,
      a.normalized
    );
  });
}

function createTextures(gltf: GLTF) {
  return (gltf.root.textures || []).map(t => {
    const sampler =
      t.sampler !== undefined && gltf.root.samplers !== undefined
        ? gltf.root.samplers[t.sampler]
        : {
            magFilter: TextureMagFilter.LINEAR,
            minFilter: TextureMinFilter.LINEAR_MIPMAP_LINEAR,
            wrapS: TextureWrap.REPEAT,
            wrapT: TextureWrap.REPEAT
          };

    if (t.source === undefined) {
      throw new Error('source is not defened for texture');
    }

    const src = gltf.images[t.source];

    return new Texture({ ...sampler, src });
  });
}

function createMaterials(gltf: GLTF, textures: Texture[]) {
  return (gltf.root.materials || []).map(m => {
    if (m.pbrMetallicRoughness === undefined) {
      throw new Error('unknown material');
    }

    const material = new PBRMaterial({
      roughness: 0.2,
      metallic: 0
    });
    if (m.pbrMetallicRoughness.baseColorFactor !== undefined) {
      material.baseColor = new Color(...m.pbrMetallicRoughness.baseColorFactor);
    }
    if (m.pbrMetallicRoughness.metallicRoughnessTexture !== undefined) {
      material.metallicRoughnessMap =
        textures[m.pbrMetallicRoughness.metallicRoughnessTexture.index];
    }
    if (m.pbrMetallicRoughness.baseColorTexture !== undefined) {
      material.baseColorMap =
        textures[m.pbrMetallicRoughness.baseColorTexture.index];
    }
    if (m.normalTexture !== undefined) {
      material.normalMap = textures[m.normalTexture.index];
    }
    if (m.doubleSided !== undefined) {
      material.doubleSided = true;
    }
    return material;
  });
}

function createMeshes(
  gltf: GLTF,
  accessors: BufferAccessor[],
  materials: PBRMaterial[]
) {
  return (gltf.root.meshes || []).map(m => {
    if (m.primitives.length > 1) {
      throw new Error('unknown situation yet');
    }
    const {
      attributes,
      indices,
      mode,
      material: materialIndex
    } = m.primitives[0];

    if (mode !== 4 && mode !== undefined) {
      throw new Error('unknown draw mode');
    }

    if (materialIndex === undefined) {
      throw new Error('material is not defened');
    }

    const attributesMap = new Map<AttributeName, BufferAccessor>();

    for (const key of Object.keys(attributes)) {
      const attrName = getAttributeName(key);
      const attrAccessor = accessors[attributes[key]];

      attributesMap.set(attrName, attrAccessor);
    }

    const indicesAccessor =
      indices !== undefined ? accessors[indices] : undefined;

    const geometry = new Geometry(attributesMap, indicesAccessor);
    const material = materials[materialIndex];
    const mesh = new Mesh(geometry, material);
    mesh.name = m.name;

    return mesh;
  });
}

function createNodes(gltf: GLTF, meshes: Mesh[]) {
  const nodes: Object3D[] = [];
  const gltfNodes = gltf.root.nodes || [];

  for (const gltfNode of gltfNodes) {
    let node: Object3D;

    if (gltfNode.mesh !== undefined) {
      node = meshes[gltfNode.mesh];
    } else {
      node = new Object3D();
    }

    if (gltfNode.children !== undefined) {
      for (const child of gltfNode.children) {
        node.add(nodes[child]);
      }
    }

    if (gltfNode.matrix !== undefined) {
      const elements = node.matrix.getElements();
      for (let i = 0; i < 16; i++) {
        elements[i] = gltfNode.matrix[i];
      }
    }

    if (gltfNode.translation !== undefined) {
      node.position.set(...gltfNode.translation);
    }

    if (gltfNode.rotation !== undefined) {
      node.quaternion.set(...gltfNode.rotation);
    }

    if (gltfNode.scale !== undefined) {
      node.scale.set(...gltfNode.scale);
    }

    node.name = gltfNode.name;

    nodes.push(node);
  }

  return nodes;
}

function createScenes(gltf: GLTF, nodes: Object3D[]) {
  return (gltf.root.scenes || []).map(s => {
    const scene = new Object3D();
    if (s.nodes !== undefined) {
      for (const node of s.nodes) {
        scene.add(nodes[node]);
      }
    }
    scene.name = s.name;
    return scene;
  });
}

function getAttributeName(gltfAttrName: string): AttributeName {
  // TODO replace attribute to enum
  switch (gltfAttrName) {
    case 'POSITION':
      return AttributeName.Position;
    case 'NORMAL':
      return AttributeName.Normal;
    case 'TEXCOORD_0':
      return AttributeName.UV;
    case 'TANGENT':
      return AttributeName.Tangent;
    default:
      throw new Error('unknown attribute name');
  }
}

function getItemSize(gltfComponentName: GLTFAccessorType): number {
  switch (gltfComponentName) {
    case GLTFAccessorType.SCALAR:
      return 1;
    case GLTFAccessorType.VEC2:
      return 2;
    case GLTFAccessorType.VEC3:
      return 3;
    case GLTFAccessorType.VEC4:
    case GLTFAccessorType.MAT2:
      return 4;
    case GLTFAccessorType.MAT3:
      return 9;
    case GLTFAccessorType.MAT4:
      return 16;
  }
}
