import merge from 'webpack-merge';

import {
  entry,
  html,
  serve,
  glsl,
  health,
  assets,
  build,
  typescript,
  styles,
  Options,
  CopyOperation,
  copier,
  Part
} from './src';

interface Init {
  root: string;
  title?: string;
  favicon?: string;
  entryPoint?: string;
  port?: number;
  copy?: CopyOperation[];
}

const preset = ({
  root,
  title = 'TowerDef',
  favicon = 'favicon.ico',
  entryPoint = './index',
  copy = [],
  port = 8080
}: Init) => {
  const debug = process.env.NODE_ENV !== 'production';
  const options: Options = {
    root,
    debug,
    title,
    favicon,
    entryPoint,
    port,
    copy
  };

  const parts: Part[] = [entry, html, typescript, glsl, health, assets, styles];

  if (debug) {
    parts.push(serve);
  }

  if (!debug) {
    parts.push(build);
  }

  if (copy.length > 0) {
    parts.push(copier);
  }

  return merge(...parts.map(p => p(options)));
};

export default preset;
