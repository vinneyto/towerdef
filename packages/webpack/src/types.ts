import { Configuration } from 'webpack';

export interface CopyOperation {
  from: string;
  to: string;
}

export interface Options {
  root: string;
  debug: boolean;
  title: string;
  favicon: string;
  entryPoint: string;
  port: number;
  copy: CopyOperation[];
}

export interface GenericConfig extends Configuration {
  devServer?: Record<string, string | number | boolean | string[]>;
}

export interface Part {
  (options: Options): GenericConfig;
}
