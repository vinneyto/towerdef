import FriendlyErrorsWebpackPlugin from 'friendly-errors-webpack-plugin';
import notifier from 'node-notifier';

import { Part } from './types';

const part: Part = () => ({
  plugins: [
    new FriendlyErrorsWebpackPlugin({
      onErrors: (severity, errors) => {
        if (severity !== 'error') {
          return;
        }
        const error = errors[0];
        notifier.notify({
          title: 'Webpack error',
          // @ts-ignore
          message: `${error.name}: ${error.file}`,
          sound: false
        });
      }
    })
  ]
});

export default part;
