import path from 'path';

import { Options, Part } from './types';

const part: Part = ({ root, debug, entryPoint }: Options) => ({
  context: path.join(root, 'src'),
  mode: debug ? 'development' : 'production',
  devtool: debug ? 'cheap-module-eval-source-map' : false,
  entry: ['whatwg-fetch', entryPoint],
  output: {
    filename: 'scripts.js',
    path: path.join(root, 'dist')
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx']
  }
});

export default part;
