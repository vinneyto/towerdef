import HtmlWebpackPlugin, { Hooks } from 'html-webpack-plugin';
import { Plugin, Compiler } from 'webpack';

import { Options, Part } from './types';

class InjectReactRootPlugin implements Plugin {
  apply(compiler: Compiler) {
    compiler.hooks.compilation.tap('InjectReactRootPlugin', compilation => {
      const hooks = compilation.hooks as Hooks;
      hooks.htmlWebpackPluginAlterAssetTags.tapAsync(
        'InjectReactRootPlugin',
        (data, cb) => {
          data.body.unshift({
            tagName: 'div',
            voidTag: false,
            attributes: {
              id: 'root'
            }
          });

          cb(null, data);
        }
      );
    });
  }
}

const part: Part = ({ debug, title, favicon }: Options) => ({
  plugins: [
    new HtmlWebpackPlugin({
      title,
      favicon,
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      hash: !debug
    }),
    new InjectReactRootPlugin()
  ]
});

export default part;
