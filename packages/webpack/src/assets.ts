import { Options, Part } from './types';

const part: Part = ({ debug }: Options) => ({
  module: {
    rules: [
      {
        test: /\.(png|jpg|zip|gltf)$/,
        use: {
          loader: 'file-loader',
          options: {
            name() {
              if (debug) {
                return '[path][name].[ext]';
              }
              return '[hash].[ext]';
            },
            outputPath: 'assets/'
          }
        }
      }
    ]
  }
});

export default part;
