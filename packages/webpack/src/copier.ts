import CopyWebpackPlugin from 'copy-webpack-plugin';

import { Part } from './types';

const part: Part = ({ copy }) => ({
  plugins: [new CopyWebpackPlugin(copy)]
});

export default part;
