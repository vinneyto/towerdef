import { Part } from './types';

const part: Part = () => ({
  module: {
    rules: [
      {
        test: /\.glsl$/,
        use: {
          loader: 'raw-loader'
        }
      }
    ]
  }
});

export default part;
