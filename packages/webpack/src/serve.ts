import path from 'path';
import { Options, Part } from './types';

const part: Part = ({ root, port }: Options) => ({
  devServer: {
    contentBase: path.join(root, 'src'),
    noInfo: true,
    open: true,
    port
  }
});

export default part;
