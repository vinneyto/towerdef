import { Part, Options } from './types';

const part: Part = ({ debug }: Options) => ({
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: debug,
          compilerOptions: {
            module: 'esnext'
          }
        }
      }
    ]
  }
});

export default part;
