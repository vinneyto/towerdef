import CleanWebpackPlugin from 'clean-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';

import { Options, Part } from './types';

const part: Part = ({ root }: Options) => ({
  performance: {
    maxEntrypointSize: 500000,
    maxAssetSize: 1000000
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], { root }),
    new CompressionPlugin({
      test: /\.js/
    })
  ]
});

export default part;
