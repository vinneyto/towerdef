import towerdef from '@towerdef/webpack';

export default () =>
  towerdef({
    title: 'Tower Defence Engine Demo',
    root: __dirname,
    copy: [{ from: 'models', to: 'models' }]
  });
