import 'normalize.css';
import {
  createSphereGeometry,
  createCubeGeometry,
  createStandardGeometry,
  StandardMaterial,
  PBRMaterial,
  GLRenderer,
  Object3D,
  RenderPass,
  Mesh,
  Color,
  Light,
  LightType,
  TurntableCameraController,
  ViewInteractor,
  Vector2,
  CubeTexture,
  EnvironmentType
  // Texture
} from '@towerdef/engine';
import { fetchGLTF, renderGLTF } from '@towerdef/gltf';
// import zeldsSrc from './assets/zelda.jpg';
import cupGeometrySrc from './assets/cup.json';
// import AvocadoGLTF from './assets/AvocadoGLTF/Avocado.gltf';
import envNegX from './assets/environment/neg-x.png';
import envNegY from './assets/environment/neg-y.png';
import envNegZ from './assets/environment/neg-z.png';
import envPosX from './assets/environment/pos-x.png';
import envPosY from './assets/environment/pos-y.png';
import envPosZ from './assets/environment/pos-z.png';

document.body.style.fontSize = '32px';
document.body.innerHTML =
  'Простите, очень долго грузится. Моделька жирная а хостинг бесплатный...';

(async () => {
  const result = await fetchGLTF('models/BoomboxGLTF/BoomBox.gltf');

  const [avocado] = renderGLTF(result);

  const scene = new Object3D();

  scene.add(avocado);
  avocado.scale.scale(9);
  avocado.position.y = 0.01;

  console.log(avocado);

  const envImages = await fetchEnvImages();

  const skyBoxTexture = new CubeTexture({
    positiveX: envImages.posX,
    negativeX: envImages.negX,
    positiveY: envImages.posY,
    negativeY: envImages.negY,
    positiveZ: envImages.posZ,
    negativeZ: envImages.negZ
  });

  avocado.traverse(obj => {
    if (obj instanceof Mesh && obj.material instanceof PBRMaterial) {
      obj.material.environmentMap = skyBoxTexture;
      obj.material.environmentMapOpacity = 0.05;
    }
  });

  console.log(result);

  const renderer = new GLRenderer();

  document.body.innerHTML = '';
  document.body.appendChild(renderer.getCanvas());

  const composer = renderer.createEffectComposer();
  const interactor = new ViewInteractor(renderer.getCanvas());
  const cameraController = new TurntableCameraController(Math.PI / 4, 0.05, 10);
  cameraController.setZoomRadius(0.3);
  cameraController.setRotation(Math.PI / 8, Math.PI / 2);

  interactor.on('dragging', (delta: Vector2) => {
    cameraController.rotateDelta(delta, 0.01);
  });

  const camera = cameraController.getCamera();

  // const geometry = new TorusGeometry(5, 1, 100, 30);
  scene.add(createAxisHelper(0.05));

  const sbGeometry = createCubeGeometry(9);
  const sbMaterial = new StandardMaterial({
    environmentMap: skyBoxTexture,
    doubleSide: true,
    skyBox: true
  });
  const skyBox = new Mesh(sbGeometry, sbMaterial);
  scene.add(skyBox);

  const sphereGeometry = createSphereGeometry(0.03, 32, 32);
  // console.log(cupGeometry);
  const cupMaterial1 = new PBRMaterial({
    baseColor: new Color().fromHex('#FEF483').toLinear(),
    roughness: 0.1,
    metallic: 0
  });
  const cup1 = new Mesh(sphereGeometry, cupMaterial1);
  // cup1.position.y = -0.03;
  cup1.position.x = -0.12;
  cup1.quaternion.fromEuler(0, 90, 0);
  // scene.add(cup1);

  // const cupMaterial2 = new Material({
  //   baseColor: new Color(1, 1, 1),
  //   environmentMap: skyBoxTexture,
  //   environmentMapOpacity: 0.05,
  //   roughness: 0.2,
  //   metallic: 0
  // });
  // const cup2 = new Mesh(cupGeometry, cupMaterial2);
  // cup2.position.y = -0.03;
  // cup2.quaternion.fromEuler(0, 90, 0);
  // scene.add(cup2);

  const cupMaterial3 = new PBRMaterial({
    environmentMapOpacity: 0.9,
    environmentMap: skyBoxTexture,
    environmentType: EnvironmentType.REFRACTION,
    refractionRatio: 0.8,
    metallic: 0.7,
    roughness: 0.2
  });

  const cup3 = new Mesh(createStandardGeometry(cupGeometrySrc), cupMaterial3);
  cup3.position.y = -0.03;
  cup3.position.x = 0.12;
  cup3.quaternion.fromEuler(0, 90, 0);
  cup3.scale.scale(0.01);
  // scene.add(cup3);

  // const image = new Image();
  // image.onload = () => {
  //   cupMaterial.map = new Texture({
  //     src: image
  //   });
  // };
  // image.src = zeldsSrc;

  // const image = new Image();
  // image.onload = () => {
  //   material.map = new Texture({ src: image });
  // };
  // image.src = zeldsSrc;

  // for (let i = -20; i <= 40; i++) {
  //   const mesh = new Mesh(geometry, material);
  //   mesh.position.x = 30;
  //   mesh.position.y = i * 3;
  //   scene.add(mesh);
  // }

  const light1 = createPointLight(new Color(1, 1, 1));
  light1.intensity = 0.25;
  light1.position.x = 0.18;
  light1.position.z = -0.02;
  light1.position.y = 0.02;

  const light2 = createPointLight(new Color(1, 1, 1));
  light2.intensity = 0.25;
  light2.position.x = 0.18;
  light2.position.z = 0.02;
  light2.position.y = -0.02;

  const light3 = createPointLight(new Color(1, 1, 1));
  light3.intensity = 0.25;
  light3.position.x = 0.18;
  light3.position.z = 0.02;
  light3.position.y = 0.02;

  const light4 = createPointLight(new Color(1, 1, 1));
  light4.intensity = 0.25;
  light4.position.x = 0.18;
  light4.position.z = -0.02;
  light4.position.y = -0.02;

  const lightContainer = new Object3D();
  lightContainer.add(light1);
  lightContainer.add(light2);
  lightContainer.add(light3);
  lightContainer.add(light4);
  scene.add(lightContainer);

  // const ambientLight = new Light({
  //   intensity: 0.05,
  //   color: new Color(1, 1, 1)
  // });
  // scene.add(ambientLight);

  const directionalLight = new Light({
    type: LightType.Directional,
    color: new Color(1, 1, 1)
  });
  directionalLight.position.set(0, 1, 1).normalize();
  directionalLight.intensity = Math.PI;
  scene.add(directionalLight);

  const directionalLight2 = new Light({
    type: LightType.Directional,
    color: new Color(1, 1, 1)
  });
  directionalLight2.position.set(0, 1, -1).normalize();
  directionalLight2.intensity = Math.PI;
  scene.add(directionalLight2);

  composer.add(
    new RenderPass({
      color: new Color(1, 1, 1),
      onPass: () => renderer.render(scene, camera)
    })
  );

  let lightsRotation = 0;

  function createSphere(color: Color) {
    return new Mesh(
      createSphereGeometry(0.003, 10, 10),
      new StandardMaterial({ baseColor: color })
    );
  }

  function createPointLight(color: Color) {
    const light = new Light({ type: LightType.Point, color });

    const sphere = createSphere(color);
    light.add(sphere);

    return light;
  }

  function createAxisHelper(size = 0.1) {
    const root = new Object3D();
    const center = createSphere(new Color(1, 1, 1).scale(0.5));
    const xs = createSphere(new Color(1, 0, 0));
    const ys = createSphere(new Color(0, 1, 0));
    const zs = createSphere(new Color(0, 0, 1));

    xs.position.x = size;
    ys.position.y = size;
    zs.position.z = size;

    root.add(center);
    root.add(xs);
    root.add(ys);
    root.add(zs);

    return root;
  }

  function fetchImage(src: string): Promise<HTMLImageElement> {
    return new Promise(resolve => {
      const image = new Image();
      image.onload = () => resolve(image);
      image.src = src;
    });
  }

  interface EnvImages {
    negX: HTMLImageElement;
    negY: HTMLImageElement;
    negZ: HTMLImageElement;
    posX: HTMLImageElement;
    posY: HTMLImageElement;
    posZ: HTMLImageElement;
  }

  async function fetchEnvImages(): Promise<EnvImages> {
    const [negX, negY, negZ, posX, posY, posZ] = await Promise.all([
      fetchImage(envNegX),
      fetchImage(envNegY),
      fetchImage(envNegZ),
      fetchImage(envPosX),
      fetchImage(envPosY),
      fetchImage(envPosZ)
    ]);

    return {
      negX,
      negY,
      negZ,
      posX,
      posY,
      posZ
    };
  }

  function loop() {
    if (renderer.hasNewSize()) {
      composer.setSize(renderer.getWidth(), renderer.getHeight());
      camera.setSize(renderer.getWidth(), renderer.getHeight());
    }

    lightsRotation += 0.3;

    lightContainer.quaternion.fromEuler(0, lightsRotation, 0);

    composer.render();

    requestAnimationFrame(loop);
  }

  loop();
})();
