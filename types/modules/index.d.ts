declare module '*.glsl' {
  const content: string;
  export default content;
}
declare module '*.svg' {
  const content: string;
  export default content;
}
declare module '*.jpg' {
  const content: string;
  export default content;
}
declare module '*.png' {
  const content: string;
  export default content;
}
declare module '*.zip' {
  const content: string;
  export default content;
}
declare module '*.gltf' {
  const content: string;
  export default content;
}
